use std::env::temp_dir;

use apply::Apply;
use camino::Utf8PathBuf;
use httpmock::MockServer;

#[tokio::test]
async fn resolve_redirects() {
    let server = MockServer::start();
    let mock1 = server.mock(|when, then| {
        when.method("HEAD").path("/file1");
        then.status(302).header("Location", "/file2");
    });
    let mock2 = server.mock(|when, then| {
        when.method("HEAD").path("/file2");
        then.status(301).header("Location", "/file3");
    });
    let mock3 = server.mock(|when, then| {
        when.method("HEAD").path("/file3");
        then.status(200);
    });

    let client = crate::new_client().unwrap();
    let base_url = server.base_url();
    let url1 = format!("{base_url}/file1");
    let url2 = format!("{base_url}/file3");
    let url = crate::resolve_redirects(&client, &url1).await.unwrap();

    mock1.assert();
    mock2.assert();
    mock3.assert();
    assert_eq!(url.to_string(), url2)
}

#[tokio::test]
async fn download_text() {
    const TEST_DATA: &str = "some test data ✖️ ✔️";
    const FILENAME: &str = "network-download_file-test";

    let server = MockServer::start();
    let mock = server.mock(|when, then| {
        when.method("GET").path("/file");
        then.status(200).body(TEST_DATA);
    });

    let client = crate::new_client().unwrap();
    let base_url = server.base_url();
    let url = format!("{base_url}/file");
    let path = temp_dir()
        .join(FILENAME)
        .apply(Utf8PathBuf::from_path_buf)
        .unwrap();
    let file = crate::download_file(&client, &url, &path).await.unwrap();
    let data = tokio::fs::read_to_string(file).await.unwrap();

    mock.assert();
    assert_eq!(data, TEST_DATA);
}

#[tokio::test]
async fn download_file() {
    const TEST_DATA: &str = "some test data ✖️ ✔️";

    let server = MockServer::start();
    let mock = server.mock(|when, then| {
        when.method("GET").path("/file");
        then.status(200).body(TEST_DATA);
    });

    let client = crate::new_client().unwrap();
    let base_url = server.base_url();
    let url = format!("{base_url}/file");
    let data = crate::download_text(&client, &url).await.unwrap();

    mock.assert();
    assert_eq!(data, TEST_DATA);
}
