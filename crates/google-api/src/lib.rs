mod youtube;

use std::iter::once;

pub use youtube::{VideoInfo, YouTubeApi};

use apply::Apply;
use pretend::{http::header::AUTHORIZATION, resolver::UrlResolver, Pretend, Url};
use pretend_reqwest::{reqwest::ClientBuilder, Client};
use thiserror::Error;
pub use yup_oauth2::storage::{TokenInfo, TokenStorage};
use yup_oauth2::{AccessToken, DeviceFlowAuthenticator};

/// GoogleAPIError enumerates all possible errors returned by this library.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum GoogleApiError {
    #[error("invalid filename")]
    InvalidFilenameError,

    #[error("failed to parse API response")]
    ApiResponseParseError,

    #[error("error parsing date: {date:?}")]
    DatetimeParseError {
        source: chrono::ParseError,
        date: String,
    },

    #[error("cannot parse an absolute URL from {0:?}")]
    UrlParseError(String),

    #[error("could not find channel: {0:?}")]
    ChannelNotFoundError(String),

    #[error("could not find playlist: {0:?}")]
    PlaylistNotFoundError(String),

    #[error("cannot parse header value: {val:?}")]
    HeaderValueParseError {
        source: pretend::http::header::InvalidHeaderValue,
        val: String,
    },

    #[error("cannot construct HTTP client")]
    ClientBuildError {
        source: pretend_reqwest::reqwest::Error,
    },

    #[error("API request failed: {0}")]
    ApiRequestError(String),

    #[error("could not get API Key from string")]
    ApplicationSecretError { source: std::io::Error },

    #[error("could not get Authenticator for token")]
    AuthenticatorError { source: std::io::Error },

    #[error("could not get Access Token")]
    TokenError { source: yup_oauth2::Error },
}

struct Api {
    base_url: Url,
    scopes: &'static [&'static str],
}

impl Api {
    pub fn new(base_url: &str, scopes: &'static [&'static str]) -> Result<Self, GoogleApiError> {
        let base_url = base_url
            .parse()
            .map_err(|_| GoogleApiError::UrlParseError(base_url.to_string()))?;
        Ok(Api { base_url, scopes })
    }

    pub async fn authenticate(
        self,
        client_secret: &str,
        token_storage: Box<dyn TokenStorage>,
    ) -> Result<Pretend<Client, UrlResolver>, GoogleApiError> {
        let token = self.get_auth_token(client_secret, token_storage).await?;

        let value = format!("Bearer {}", token.as_str())
            .parse()
            .map_err(|source| GoogleApiError::HeaderValueParseError {
                source,
                val: token.as_str().to_string(),
            })?;

        let headers = once((AUTHORIZATION, value)).collect();

        ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(|source| GoogleApiError::ClientBuildError { source })?
            .apply(Client::new)
            .apply(Pretend::for_client)
            .with_url(self.base_url)
            .apply(Ok)
    }

    /// gets an auth token
    async fn get_auth_token(
        &self,
        client_secret: &str,
        token_storage: Box<dyn TokenStorage>,
    ) -> Result<AccessToken, GoogleApiError> {
        let secret = yup_oauth2::parse_application_secret(client_secret)
            .map_err(|source| GoogleApiError::ApplicationSecretError { source })?;
        // Create an authenticator that uses an DeviceFlow to authenticate.
        // The authentication tokens are persisted to the token storage.
        // The authenticator takes care of caching tokens and refreshing tokens once they've expired.
        let authenticator = DeviceFlowAuthenticator::builder(secret)
            .with_storage(token_storage)
            .build()
            .await
            .map_err(|source| GoogleApiError::AuthenticatorError { source })?;

        // does everything to obtain a token that can be sent as Bearer token.
        authenticator
            .token(self.scopes)
            .await
            .map_err(|source| GoogleApiError::TokenError { source })
    }
}

trait IntoString {
    fn into_string(self) -> String;
}

impl IntoString for pretend::Error {
    fn into_string(self) -> String {
        match self {
            pretend::Error::Client(_) => "Failed to create client".to_string(),
            pretend::Error::Request(_) => "Invalid request".to_string(),
            pretend::Error::Response(_) => "Failed to execute request".to_string(),
            pretend::Error::Body(_) => "Failed to read response body".to_string(),
            pretend::Error::Status(s) => format!("HTTP {}", s),
        }
    }
}
