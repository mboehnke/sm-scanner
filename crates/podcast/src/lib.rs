use apply::Apply;
use chrono::{DateTime, Duration, Utc};
use network::{Client, NetworkError};
use rss::{Channel, Item};
use sm_api::model::Podcast;
use thiserror::Error;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum PodcastError {
    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error("could not parse rss feed: {url:?}")]
    FeedParseError { source: rss::Error, url: String },
}

/// gets up to `n` episodes from `url` that are at most `days` old
pub async fn get(url: &str, max_days: u32, max_num: u32) -> Result<Vec<Podcast>, PodcastError> {
    let client = network::new_client()?;
    get_with_client(&client, url, max_days, max_num).await
}

pub async fn get_with_client(
    client: &Client,
    url: &str,
    max_days: u32,
    max_num: u32,
) -> Result<Vec<Podcast>, PodcastError> {
    let channel = network::download_text(client, url)
        .await?
        .parse::<Channel>()
        .map_err(|source| PodcastError::FeedParseError {
            source,
            url: url.to_string(),
        })?;

    let now = Utc::now();

    let podcasts = channel
        .items()
        .iter()
        .take(max_num.try_into().unwrap_or_default())
        .filter_map(|item| parse_item(item, &channel))
        // fewer than `days` old
        .filter(|i| {
            i.upload_date
                .checked_add_signed(Duration::days(max_days.into()))
                .unwrap_or(i.upload_date)
                > now
        })
        .collect();

    Ok(podcasts)
}

fn parse_item(item: &Item, channel: &Channel) -> Option<Podcast> {
    // get published date
    let upload_date = item
        .pub_date()?
        .apply(DateTime::parse_from_rfc2822)
        .ok()?
        .into();
    let audio = item
        .enclosure()
        .filter(|e| e.mime_type().contains("audio"))?
        .url()
        .split('?')
        .next()?
        .to_string();
    let thumbnail = item
        .itunes_ext()?
        .image()
        // default to channel image if no episode image can be found
        .or_else(|| channel.image().map(|img| img.url()))
        .or_else(|| channel.itunes_ext().and_then(|e| e.image()))?
        // delete any post data in url
        .split('?')
        .next()?
        .to_string();

    Some(Podcast {
        channel: channel.title().to_string(),
        title: item.title()?.to_string(),
        description: item.description().unwrap_or_default().to_string(),
        upload_date,
        audio,
        thumbnail,
    })
}
