use sm_api::{model::TwitchItem, scanner::*};
use tonic::{Request, Response, Status};
use tracing::{debug, info, instrument};

use crate::AppError;

#[derive(Clone, Debug, Default)]
pub struct TwitchService {}

#[tonic::async_trait]
impl sm_api::scanner::twitch_server::Twitch for TwitchService {
    #[instrument(skip_all)]
    async fn uploads(
        &self,
        request: Request<TwitchUploadsRequest>,
    ) -> Result<Response<TwitchUploadsReply>, Status> {
        let TwitchUploadsRequest {
            channel_id,
            max_days,
            max_num,
        } = request.into_inner();

        debug!(%channel_id, %max_days, %max_num, "getting latest uploads");

        let items = twitch::get(&channel_id, max_days, max_num)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(TwitchItem::from)
            .collect::<Vec<_>>();

        info!(%channel_id, "collected list of {} latest uploads", items.len());

        Ok(Response::new(TwitchUploadsReply { items }))
    }
}
