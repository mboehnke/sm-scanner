use std::net::Ipv4Addr;

pub(crate) const DS_PORT_SETTING: &str = "SM_DATASTORE_PORT";
pub(crate) const DS_PORT_DEFAULT: u16 = 50051;

pub(crate) const DS_IP_SETTING: &str = "SM_DATASTORE_IP";
pub(crate) const DS_IP_DEFAULT: Ipv4Addr = Ipv4Addr::new(127, 0, 0, 1);

pub(crate) const SETTINGS_TREE: &str = "SC_SETTINGS";
pub(crate) const LOG_TREE: &str = "SC_LOG";
pub(crate) const CREDENTIALS_TREE: &str = "CREDENTIALS_TREE";

pub(crate) const PORT_SETTING: &str = "SM_SCANNER_PORT";
pub(crate) const PORT_DEFAULT: u16 = 50052;

pub(crate) const IP_SETTING: &str = "SM_SCANNER_IP";
pub(crate) const IP_DEFAULT: Ipv4Addr = Ipv4Addr::new(127, 0, 0, 1);

pub(crate) const LOG_LEVEL_SETTING: &str = "SM_SCANNER_LOG_LEVEL";
pub(crate) const LOG_LEVEL_DEFAULT: &str = "WARN";

pub(crate) const LOG_KEEP_DAYS_SETTING: &str = "SM_SCANNER_LOG_KEEP_DAYS";
pub(crate) const LOG_KEEP_DAYS_DEFAULT: u8 = 2;

pub(crate) const TOKEN_SETTING: &str = "SM_SCANNER_GOOGLE_API_TOKEN";
pub(crate) const CLIENT_SECRET_SETTING: &str = "SM_SCANNER_GOOGLE_API_CLIENT_SECRET";
