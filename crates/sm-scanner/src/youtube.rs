use sm_api::{model::YouTubeItem, scanner::*};
use tonic::{Request, Response, Status};
use tracing::{debug, info, instrument};

use crate::{datastore::YouTubeCredentials, AppError};

#[derive(Clone, Debug)]
pub struct YouTubeService(pub YouTubeCredentials);

#[tonic::async_trait]
impl sm_api::scanner::you_tube_server::YouTube for YouTubeService {
    #[instrument(skip_all)]
    async fn uploads(
        &self,
        request: Request<YouTubeUploadsRequest>,
    ) -> Result<Response<YouTubeUploadsReply>, Status> {
        let YouTubeUploadsRequest {
            channel_id,
            max_days,
            max_num,
        } = request.into_inner();

        debug!(%channel_id, %max_days, %max_num, "getting latest uploads from rss feed");

        let items = youtube::rss::get(&channel_id, max_days, max_num)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(YouTubeItem::from)
            .collect::<Vec<_>>();

        info!(
            %channel_id,
            "collected list of {} latest uploads from rss feed",
            items.len()
        );

        Ok(Response::new(YouTubeUploadsReply { items }))
    }

    #[instrument(skip_all)]
    async fn subscribed_channels(
        &self,
        _request: Request<YouTubeSubscribedChannelsRequest>,
    ) -> Result<Response<YouTubeSubscribedChannelsReply>, Status> {
        debug!("getting subscribed channels from youtube api");

        let client_secret = self
            .0
            .get_client_secret()
            .await?
            .ok_or_else(|| AppError("youtube api client secret not found".into()))?;
        let token_storage = Box::new(self.0.clone());

        let channel_ids = youtube::api::subscribed_channels(&client_secret, token_storage)
            .await
            .map_err(AppError::from)?;

        info!(
            "collected list of {} subscribed channels from youtube api",
            channel_ids.len()
        );

        Ok(Response::new(YouTubeSubscribedChannelsReply {
            channel_ids,
        }))
    }

    #[instrument(skip_all)]
    async fn playlist(
        &self,
        request: Request<YouTubePlaylistRequest>,
    ) -> Result<Response<YouTubePlaylistReply>, Status> {
        let YouTubePlaylistRequest { playlist_name } = request.into_inner();

        debug!(%playlist_name, "getting playlist items");

        let client_secret = self
            .0
            .get_client_secret()
            .await?
            .ok_or_else(|| AppError("youtube api client secret not found".into()))?;
        let token_storage = Box::new(self.0.clone());

        let items = youtube::api::playlist_items(&client_secret, token_storage, &playlist_name)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(|(playlist_item_id, v)| YouTubePlaylistItem {
                playlist_item_id,
                item: Some(v.into()),
            })
            .collect::<Vec<_>>();

        info!(%playlist_name, "collected list of {} playlist items", items.len());

        Ok(Response::new(YouTubePlaylistReply { items }))
    }

    #[instrument(skip_all)]
    async fn remove_playlist_item(
        &self,
        request: Request<YouTubeRemovePlaylistItemRequest>,
    ) -> Result<Response<YouTubeRemovePlaylistItemReply>, Status> {
        let YouTubeRemovePlaylistItemRequest { playlist_item_id } = request.into_inner();

        debug!(%playlist_item_id, "deleting playlist item");

        let client_secret = self
            .0
            .get_client_secret()
            .await?
            .ok_or_else(|| AppError("youtube api client secret not found".into()))?;
        let token_storage = Box::new(self.0.clone());

        youtube::api::remove_playlist_item(&client_secret, token_storage, &playlist_item_id)
            .await
            .map_err(AppError::from)?;

        info!(%playlist_item_id, "deleted playlist item");

        Ok(Response::new(YouTubeRemovePlaylistItemReply {}))
    }

    #[instrument(skip_all)]
    async fn all_uploads(
        &self,
        request: Request<YouTubeAllUploadsRequest>,
    ) -> Result<Response<YouTubeAllUploadsReply>, Status> {
        let YouTubeAllUploadsRequest {
            max_days,
            max_num_per_channel,
        } = request.into_inner();

        debug!(
            %max_days,
            %max_num_per_channel,
            "getting latest uploads of all subscribed channels from youtube api"
        );

        let client_secret = self
            .0
            .get_client_secret()
            .await?
            .ok_or_else(|| AppError("youtube api client secret not found".into()))?;
        let token_storage = Box::new(self.0.clone());

        let items = youtube::api::subscription_uploads(
            &client_secret,
            token_storage,
            max_days,
            max_num_per_channel,
        )
        .await
        .map_err(AppError::from)?
        .into_iter()
        .map(YouTubeItem::from)
        .collect::<Vec<_>>();

        info!(
            "collected list of {} latest uploads of all subscribed channels from youtube api",
            items.len()
        );

        Ok(Response::new(YouTubeAllUploadsReply { items }))
    }

    async fn set_client_secret(
        &self,
        request: Request<YouTubeSetClientSecretRequest>,
    ) -> Result<Response<YouTubeSetClientSecretReply>, Status> {
        let YouTubeSetClientSecretRequest { secret } = request.into_inner();

        debug!("updating youtube api client secret");

        self.0
            .set_client_secret(secret)
            .await
            .map_err(AppError::from)?;

        info!("updated youtube api client secret");

        Ok(Response::new(YouTubeSetClientSecretReply {}))
    }
}
