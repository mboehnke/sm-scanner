use sm_api::client::datastore::{Datastore, Tree};
use tracing::{info, instrument};
use youtube::{TokenInfo, TokenStorage};

use crate::{constants::*, AppError};

#[derive(Debug, Clone)]
pub struct YouTubeCredentials(pub Tree);

#[instrument(skip_all)]
pub async fn connect() -> Result<Datastore, AppError> {
    let ip = std::env::var(DS_IP_SETTING)
        .ok()
        .and_then(|s| s.parse().ok())
        .unwrap_or(DS_IP_DEFAULT);
    let port = std::env::var(DS_PORT_SETTING)
        .ok()
        .and_then(|s| s.parse().ok())
        .unwrap_or(DS_PORT_DEFAULT);
    let dst = format!("{ip}:{port}");

    info!(%dst, "connecting to datastore");

    Datastore::connect(dst).await.map_err(AppError::from)
}

#[tonic::async_trait]
impl TokenStorage for YouTubeCredentials {
    async fn set(&self, _scopes: &[&str], token: TokenInfo) -> anyhow::Result<()> {
        self.0.insert(TOKEN_SETTING, token).await?;
        Ok(())
    }

    async fn get(&self, _scopes: &[&str]) -> Option<TokenInfo> {
        self.0.get(TOKEN_SETTING).await.ok()?
    }
}

impl YouTubeCredentials {
    pub async fn set_client_secret(&self, client_secret: String) -> Result<(), AppError> {
        self.0
            .insert(CLIENT_SECRET_SETTING, client_secret)
            .await
            .map(|_| ())
            .map_err(AppError::from)
    }

    pub async fn get_client_secret(&self) -> Result<Option<String>, AppError> {
        self.0
            .get(CLIENT_SECRET_SETTING)
            .await
            .map_err(AppError::from)
    }
}
