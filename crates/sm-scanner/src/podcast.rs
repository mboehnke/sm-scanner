use sm_api::{model::PodcastItem, scanner::*};
use tonic::{Request, Response, Status};
use tracing::{debug, info, instrument};

use crate::AppError;

#[derive(Clone, Debug, Default)]
pub struct PodcastService {}

#[tonic::async_trait]
impl sm_api::scanner::podcast_server::Podcast for PodcastService {
    #[instrument(skip_all)]
    async fn uploads(
        &self,
        request: Request<PodcastUploadsRequest>,
    ) -> Result<Response<PodcastUploadsReply>, Status> {
        let PodcastUploadsRequest {
            url,
            max_days,
            max_num,
        } = request.into_inner();

        debug!(%url, %max_days, %max_num, "getting latest uploads");

        let items = podcast::get(&url, max_days, max_num)
            .await
            .map_err(AppError::from)?
            .into_iter()
            .map(PodcastItem::from)
            .collect::<Vec<_>>();

        info!(%url, "collected list of {} latest uploads", items.len());

        Ok(Response::new(PodcastUploadsReply { items }))
    }
}
