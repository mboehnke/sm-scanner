mod constants;
mod datastore;
mod podcast;
mod twitch;
mod youtube;

use std::net::SocketAddr;

use crate::{
    constants::*, datastore::YouTubeCredentials, podcast::PodcastService, twitch::TwitchService,
    youtube::YouTubeService,
};

use apply::Apply;
use chrono::{Duration, Utc};
use sm_api::{
    client::datastore::{DatastoreWriter, Tree},
    model::LogEntry,
    scanner::{
        podcast_server::PodcastServer, twitch_server::TwitchServer, you_tube_server::YouTubeServer,
    },
};
use tonic::{transport::Server, Status};
use tracing::{error, info, metadata::LevelFilter};

#[derive(Clone, Debug)]
pub struct AppError(String);

#[tokio::main]
async fn main() -> Result<(), AppError> {
    let datastore = datastore::connect()
        .await
        .expect("could not connect to datastore");

    let settings = datastore.tree(SETTINGS_TREE);
    let log = datastore.tree(LOG_TREE);
    let credentials = YouTubeCredentials(datastore.tree(CREDENTIALS_TREE));

    let log_level: LevelFilter = settings
        .get_or_insert(LOG_LEVEL_SETTING, LOG_LEVEL_DEFAULT.to_string())
        .await?
        .parse()
        .map_err(AppError::from)?;

    let log_writer = DatastoreWriter::new(log.clone())?;
    let make_writer = move || log_writer.clone();

    tracing_subscriber::fmt()
        .json()
        .with_max_level(log_level)
        .with_writer(make_writer)
        .init();

    let log_keep_days = settings
        .get_or_insert(LOG_KEEP_DAYS_SETTING, LOG_KEEP_DAYS_DEFAULT)
        .await?;

    if let Err(AppError(e)) = delete_old_logs(log, log_keep_days).await {
        eprintln!("error while deleting old logs:\n{e}")
    }

    let ip = settings.get_or_insert(IP_SETTING, IP_DEFAULT).await?;
    let port = settings.get_or_insert(PORT_SETTING, PORT_DEFAULT).await?;
    let addr = SocketAddr::from((ip, port));

    let twitch_svc = TwitchService::default().apply(TwitchServer::new);
    let podcast_svc = PodcastService::default().apply(PodcastServer::new);
    let youtube_svc = YouTubeService(credentials).apply(YouTubeServer::new);

    info!(%addr, "starting server");

    Server::builder()
        .trace_fn(|_| tracing::info_span!("sub-scanner"))
        .add_service(twitch_svc)
        .add_service(podcast_svc)
        .add_service(youtube_svc)
        .serve(addr)
        .await
        .map_err(|e| {
            let e = AppError::from(e);
            error!("{}", e.0);
            e
        })
}

async fn delete_old_logs(log: Tree, keep_days: u8) -> Result<(), AppError> {
    let cutoff = Utc::now() - Duration::days(keep_days as i64);

    let too_old = log.get_all_raw().await?.into_iter().filter_map(|(k, v)| {
        (String::from_utf8(v)
            .ok()?
            .parse::<LogEntry>()
            .ok()?
            .timestamp
            < cutoff)
            .then(|| k)
    });

    for key in too_old {
        log.remove_raw(key).await?;
    }

    Ok(())
}

impl<T: std::error::Error> From<T> for AppError {
    fn from(err: T) -> Self {
        let mut msg = format!("Error: {err}");
        let mut source = err.source();
        while let Some(e) = source {
            msg.push_str(&format!("\nCaused by: {e}"));
            source = e.source();
        }

        Self(msg.trim().into())
    }
}

impl From<AppError> for Status {
    fn from(e: AppError) -> Self {
        error!("{}", e.0);
        Self::internal(e.0)
    }
}

impl From<AppError> for anyhow::Error {
    fn from(e: AppError) -> Self {
        anyhow::format_err!("{}", e.0)
    }
}
