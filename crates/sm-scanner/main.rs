use api::{
    ChannelUploadsRequest, PodcastItem, PodcastUploadsReply, TwitchItem, TwitchUploadsReply,
};
use camino::Utf8PathBuf;
use tonic::{transport::Server, Request, Response, Status};

#[derive(Debug, Default)]
pub struct SubScanner {
    client_secret: Utf8PathBuf,
    token: Utf8PathBuf,
}

#[tonic::async_trait]
impl api::twitch_server::Twitch for SubScanner {
    async fn uploads(
        &self,
        req: Request<ChannelUploadsRequest>,
    ) -> Result<Response<TwitchUploadsReply>, Status> {
        let ChannelUploadsRequest { channel, days, n } = req.into_inner();
        let items = twitch::get(&channel, days as u8, n as u8)
            .await
            .unwrap()
            .into_iter()
            .map(|v| TwitchItem {
                id: v.id,
                upload_date: v.upload_date.timestamp(),
                channel: channel.clone(),
                title: v.title,
            })
            .collect::<Vec<_>>();
        Ok(Response::new(TwitchUploadsReply { items }))
    }
}

#[tonic::async_trait]
impl api::podcast_server::Podcast for SubScanner {
    async fn uploads(
        &self,
        req: Request<ChannelUploadsRequest>,
    ) -> Result<Response<PodcastUploadsReply>, Status> {
        let ChannelUploadsRequest { channel, days, n } = req.into_inner();
        let items = podcast::get(&channel, days as u8, n as u8)
            .await
            .unwrap()
            .into_iter()
            .map(|p| PodcastItem {
                audio: p.audio,
                thumbnail: p.thumbnail,
                upload_date: p.upload_date.timestamp(),
                channel: p.channel,
                title: p.title,
                description: p.description,
            })
            .collect::<Vec<_>>();
        Ok(Response::new(PodcastUploadsReply { items }))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "127.0.0.1:50051".parse()?;
    let scanner = SubScanner::default();

    Server::builder()
        .add_service(api::twitch_server::TwitchServer::new(scanner))
        .serve(addr)
        .await?;

    Ok(())
}
