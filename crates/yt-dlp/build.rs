#[path = "build/mod.rs"]
mod build;

use std::{fs::File, io::Write, path::PathBuf, process::Command};

fn main() {
    let file = PathBuf::from("src/ytdlp.rs");
    if file.is_file() {
        return;
    }
    File::create(&file)
        .unwrap()
        .write_all(build::code().unwrap().as_bytes())
        .unwrap();
    Command::new("rustfmt")
        .args([file.to_str().unwrap(), "--edition", "2021"])
        .status()
        .unwrap()
        .success()
        .then(|| ())
        .unwrap()
}
