//! auto-generated code
//! do not edit

#[derive(Clone, Debug)]
pub enum YtDlpOption {
    /// Print this help text and exit
    Help,
    /// Print program version and exit
    Version,
    /// Update this program to latest version. Make sure that you have sufficient permissions (run with sudo if needed)
    Update,
    /// Ignore download and postprocessing errors. The download will be considered successful even if the postprocessing fails
    IgnoreErrors,
    /// Continue with next video on download errors; e.g. to skip unavailable videos in a playlist (default)
    NoAbortOnError,
    /// Abort downloading of further videos if an error occurs (Alias: --no-ignore-errors)
    AbortOnError,
    /// Display the current user-agent and exit
    DumpUserAgent,
    /// List all supported extractors and exit
    ListExtractors,
    /// Output descriptions of all supported extractors and exit
    ExtractorDescriptions,
    /// Force extraction to use the generic extractor
    ForceGenericExtractor,
    /// Use this prefix for unqualified URLs. For example "gvsearch2:" downloads two videos from google videos for the search term "large apple". Use the value "auto" to let yt-dlp guess ("auto_warning" to emit a warning when guessing). "error" just throws an error. The default value "fixup_error" repairs broken URLs, but emits an error if this is not possible instead of searching
    DefaultSearch(String),
    /// Don't load any more configuration files except those given by --config-locations. For backward compatibility, if this option is found inside the system configuration file, the user configuration is not loaded. (Alias: --no-config)
    IgnoreConfig,
    /// Do not load any custom configuration files (default). When given inside a configuration file, ignore all previous --config-locations defined in the current file
    NoConfigLocations,
    /// Location of the main configuration file; either the path to the config or its containing directory. Can be used multiple times and inside other configuration files
    ConfigLocations(String),
    /// Do not extract the videos of a playlist, only list them
    FlatPlaylist,
    /// Extract the videos of a playlist
    NoFlatPlaylist,
    /// Download livestreams from the start. Currently only supported for YouTube (Experimental)
    LiveFromStart,
    /// Download livestreams from the current time (default)
    NoLiveFromStart,
    /// MIN[-MAX]       Wait for scheduled streams to become available. Pass the minimum number of seconds (or range) to wait between retries
    WaitForVideo,
    /// Do not wait for scheduled streams (default)
    NoWaitForVideo,
    /// Mark videos watched (even with --simulate)
    MarkWatched,
    /// Do not mark videos watched (default)
    NoMarkWatched,
    /// Do not emit color codes in output
    NoColors,
    /// Options that can help keep compatibility with youtube-dl or youtube-dlc configurations by reverting some of the changes made in yt-dlp. See "Differences in default behavior" for details
    CompatOptions(String),
    /// Use the specified HTTP/HTTPS/SOCKS proxy. To enable SOCKS proxy, specify a proper scheme. For example socks5://user:pass@127.0.0.1:1080/. Pass in an empty string (--proxy "") for direct connection
    Proxy(String),
    /// Time to wait before giving up, in seconds
    SocketTimeout(String),
    /// Client-side IP address to bind to
    SourceAddress(String),
    /// Make all connections via IPv4
    ForceIpv4,
    /// Make all connections via IPv6
    ForceIpv6,
    /// Use this proxy to verify the IP address for some geo-restricted sites. The default proxy specified by --proxy (or none, if the option is not present) is used for the actual downloading
    GeoVerificationProxy(String),
    /// Bypass geographic restriction via faking X-Forwarded-For HTTP header (default)
    GeoBypass,
    /// Do not bypass geographic restriction via faking X-Forwarded-For HTTP header
    NoGeoBypass,
    /// Force bypass geographic restriction with explicitly provided two-letter ISO 3166-2 country code
    GeoBypassCountry(String),
    /// IP_BLOCK   Force bypass geographic restriction with explicitly provided IP block in CIDR notation
    GeoBypassIpBlock,
    /// Playlist video to start at (default is 1)
    PlaylistStart(String),
    /// Playlist video to end at (default is last)
    PlaylistEnd(String),
    /// ITEM_SPEC       Playlist video items to download. Specify indices of the videos in the playlist separated by commas like: "--playlist-items 1,2,5,8" if you want to download videos indexed 1, 2, 5, 8 in the playlist. You can specify range: "--playlist-items 1-3,7,10-13", it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13
    PlaylistItems,
    /// Do not download any videos smaller than SIZE (e.g. 50k or 44.6m)
    MinFilesize(String),
    /// Do not download any videos larger than SIZE (e.g. 50k or 44.6m)
    MaxFilesize(String),
    /// Download only videos uploaded on this date. The date can be "YYYYMMDD" or in the format "(now|today)[+-][0- 9](day|week|month|year)(s)?"
    Date(String),
    /// Download only videos uploaded on or before this date. The date formats accepted is the same as --date
    Datebefore(String),
    /// Download only videos uploaded on or after this date. The date formats accepted is the same as --date
    Dateafter(String),
    /// Generic video filter. Any field (see "OUTPUT TEMPLATE") can be compared with a number or a string using the operators defined in "Filtering formats". You can also simply specify a field to match if the field is present and "!field" to check if the field is not present. In addition, Python style regular expression matching can be done using "~=", and multiple filters can be checked with "&". Use a "\" to escape "&" or quotes if needed. Eg: --match-filter "!is_live & like_count>?100 & description~='(?i)\bcats \& dogs\b'" matches only videos that are not live, has a like count more than 100 (or the like field is not available), and also has a description that contains the phrase "cats & dogs" (ignoring case)
    MatchFilter(String),
    /// Do not use generic video filter (default)
    NoMatchFilter,
    /// Download only the video, if the URL refers to a video and a playlist
    NoPlaylist,
    /// Download the playlist, if the URL refers to a video and a playlist
    YesPlaylist,
    /// Download only videos suitable for the given age
    AgeLimit(String),
    /// Download only videos not listed in the archive file. Record the IDs of all downloaded videos in it
    DownloadArchive(String),
    /// Do not use archive file (default)
    NoDownloadArchive,
    /// Abort after downloading NUMBER files
    MaxDownloads(String),
    /// Stop the download process when encountering a file that is in the archive
    BreakOnExisting,
    /// Stop the download process when encountering a file that has been filtered out
    BreakOnReject,
    /// Make --break-on-existing and --break-on- reject act only on the current input URL
    BreakPerInput,
    /// --break-on-existing and --break-on-reject terminates the entire download queue
    NoBreakPerInput,
    /// Number of allowed failures until the rest of the playlist is skipped
    SkipPlaylistAfterErrors(String),
    /// Number of fragments of a dash/hlsnative video that should be downloaded concurrently (default is 1)
    ConcurrentFragments(String),
    /// Maximum download rate in bytes per second (e.g. 50K or 4.2M)
    LimitRate(String),
    /// Minimum download rate in bytes per second below which throttling is assumed and the video data is re-extracted (e.g. 100K)
    ThrottledRate(String),
    /// Number of retries (default is 10), or "infinite"
    Retries(String),
    /// Number of times to retry on file access error (default is 3), or "infinite"
    FileAccessRetries(String),
    /// Number of retries for a fragment (default is 10), or "infinite" (DASH, hlsnative and ISM)
    FragmentRetries(String),
    /// Skip unavailable fragments for DASH, hlsnative and ISM (default) (Alias: --no- abort-on-unavailable-fragment)
    SkipUnavailableFragments,
    /// Abort downloading if a fragment is unavailable (Alias: --no-skip-unavailable- fragments)
    AbortOnUnavailableFragment,
    /// Keep downloaded fragments on disk after downloading is finished
    KeepFragments,
    /// Delete downloaded fragments after downloading is finished (default)
    NoKeepFragments,
    /// Size of download buffer (e.g. 1024 or 16K) (default is 1024)
    BufferSize(String),
    /// The buffer size is automatically resized from an initial value of --buffer-size (default)
    ResizeBuffer,
    /// Do not automatically adjust the buffer size
    NoResizeBuffer,
    /// Size of a chunk for chunk-based HTTP downloading (e.g. 10485760 or 10M) (default is disabled). May be useful for bypassing bandwidth throttling imposed by a webserver (experimental)
    HttpChunkSize(String),
    /// Download playlist videos in reverse order
    PlaylistReverse,
    /// Download playlist videos in default order (default)
    NoPlaylistReverse,
    /// Download playlist videos in random order
    PlaylistRandom,
    /// Set file xattribute ytdl.filesize with expected file size
    XattrSetFilesize,
    /// Use the mpegts container for HLS videos; allowing some players to play the video while downloading, and reducing the chance of file corruption if download is interrupted. This is enabled by default for live streams
    HlsUseMpegts,
    /// Do not use the mpegts container for HLS videos. This is default when not downloading live streams
    NoHlsUseMpegts,
    /// Name or path of the external downloader to use (optionally) prefixed by the protocols (http, ftp, m3u8, dash, rstp, rtmp, mms) to use it for. Currently supports native, aria2c, avconv, axel, curl, ffmpeg, httpie, wget (Recommended: aria2c). You can use this option multiple times to set different downloaders for different protocols. For example, --downloader aria2c --downloader "dash,m3u8:native" will use aria2c for http/ftp downloads, and the native downloader for dash/m3u8 downloads (Alias: --external-downloader)
    Downloader(String),
    /// Give these arguments to the external downloader. Specify the downloader name and the arguments separated by a colon ":". For ffmpeg, arguments can be passed to different positions using the same syntax as --postprocessor-args. You can use this option multiple times to give different arguments to different downloaders (Alias: --external-downloader-args)
    DownloaderArgs(String),
    /// File containing URLs to download ("-" for stdin), one URL per line. Lines starting with "#", ";" or "]" are considered as comments and ignored
    BatchFile(String),
    /// Do not read URLs from batch file (default)
    NoBatchFile,
    /// The paths where the files should be downloaded. Specify the type of file and the path separated by a colon ":". All the same TYPES as --output are supported. Additionally, you can also provide "home" (default) and "temp" paths. All intermediary files are first downloaded to the temp path and then the final files are moved over to the home path after download is finished. This option is ignored if --output is an absolute path
    Paths(String),
    /// Output filename template; see "OUTPUT TEMPLATE" for details
    Output(String),
    /// Placeholder value for unavailable meta fields in output filename template (default: "NA")
    OutputNaPlaceholder(String),
    /// Restrict filenames to only ASCII characters, and avoid "&" and spaces in filenames
    RestrictFilenames,
    /// Allow Unicode characters, "&" and spaces in filenames (default)
    NoRestrictFilenames,
    /// Force filenames to be Windows-compatible
    WindowsFilenames,
    /// Make filenames Windows-compatible only if using Windows (default)
    NoWindowsFilenames,
    /// Limit the filename length (excluding extension) to the specified number of characters
    TrimFilenames(String),
    /// Do not overwrite any files
    NoOverwrites,
    /// Overwrite all video and metadata files. This option includes --no-continue
    ForceOverwrites,
    /// Do not overwrite the video, but overwrite related files (default)
    NoForceOverwrites,
    /// Resume partially downloaded files/fragments (default)
    Continue,
    /// Do not resume partially downloaded fragments. If the file is not fragmented, restart download of the entire file
    NoContinue,
    /// Use .part files instead of writing directly into output file (default)
    Part,
    /// Do not use .part files - write directly into output file
    NoPart,
    /// Use the Last-modified header to set the file modification time (default)
    Mtime,
    /// Do not use the Last-modified header to set the file modification time
    NoMtime,
    /// Write video description to a .description file
    WriteDescription,
    /// Do not write video description (default)
    NoWriteDescription,
    /// Write video metadata to a .info.json file (this may contain personal information)
    WriteInfoJson,
    /// Do not write video metadata (default)
    NoWriteInfoJson,
    /// Write playlist metadata in addition to the video metadata when using --write-info- json, --write-description etc. (default)
    WritePlaylistMetafiles,
    /// Do not write playlist metadata when using --write-info-json, --write-description etc.
    NoWritePlaylistMetafiles,
    /// Remove some private fields such as filenames from the infojson. Note that it could still contain some personal information (default)
    CleanInfoJson,
    /// Write all fields to the infojson
    NoCleanInfoJson,
    /// Retrieve video comments to be placed in the infojson. The comments are fetched even without this option if the extraction is known to be quick (Alias: --get-comments)
    WriteComments,
    /// Do not retrieve video comments unless the extraction is known to be quick (Alias: --no-get-comments)
    NoWriteComments,
    /// JSON file containing the video information (created with the "--write-info-json" option)
    LoadInfoJson(String),
    /// Netscape formatted file to read cookies from and dump cookie jar in
    Cookies(String),
    /// Do not read/dump cookies from/to file (default)
    NoCookies,
    /// BROWSER[+KEYRING][:PROFILE] The name of the browser and (optionally) the name/path of the profile to load cookies from, separated by a ":". Currently supported browsers are: brave, chrome, chromium, edge, firefox, opera, safari, vivaldi. By default, the most recently accessed profile is used. The keyring used for decrypting Chromium cookies on Linux can be (optionally) specified after the browser name separated by a "+". Currently supported keyrings are: basictext, gnomekeyring, kwallet
    CookiesFromBrowser,
    /// Do not load cookies from browser (default)
    NoCookiesFromBrowser,
    /// Location in the filesystem where youtube-dl can store some downloaded information (such as client ids and signatures) permanently. By default $XDG_CACHE_HOME/yt-dlp or ~/.cache/yt-dlp
    CacheDir(String),
    /// Disable filesystem caching
    NoCacheDir,
    /// Delete all filesystem cache files
    RmCacheDir,
    /// Write thumbnail image to disk
    WriteThumbnail,
    /// Do not write thumbnail image to disk (default)
    NoWriteThumbnail,
    /// Write all thumbnail image formats to disk
    WriteAllThumbnails,
    /// List available thumbnails of each video. Simulate unless --no-simulate is used
    ListThumbnails,
    /// Write an internet shortcut file, depending on the current platform (.url, .webloc or .desktop). The URL may be cached by the OS
    WriteLink,
    /// Write a .url Windows internet shortcut. The OS caches the URL based on the file path
    WriteUrlLink,
    /// Write a .webloc macOS internet shortcut
    WriteWeblocLink,
    /// Write a .desktop Linux internet shortcut
    WriteDesktopLink,
    /// Activate quiet mode. If used with --verbose, print the log to stderr
    Quiet,
    /// Ignore warnings
    NoWarnings,
    /// Do not download the video and do not write anything to disk
    Simulate,
    /// Download the video even if printing/listing options are used
    NoSimulate,
    /// Ignore "No video formats" error. Useful for extracting metadata even if the videos are not actually available for download (experimental)
    IgnoreNoFormatsError,
    /// Throw error when no downloadable video formats are found (default)
    NoIgnoreNoFormatsError,
    /// Do not download the video but write all related files (Alias: --no-download)
    SkipDownload,
    /// Field name or output template to print to screen, optionally prefixed with when to print it, separated by a ":". Supported values of "WHEN" are the same as that of --use-postprocessor, and "video" (default). Implies --quiet and --simulate (unless --no-simulate is used). This option can be used multiple times
    Print(String),
    /// Append given template to the file. The values of WHEN and TEMPLATE are same as that of --print. FILE uses the same syntax as the output template. This option can be used multiple times
    PrintToFile(String, String),
    /// Quiet, but print JSON information for each video. Simulate unless --no-simulate is used. See "OUTPUT TEMPLATE" for a description of available keys
    DumpJson,
    /// Quiet, but print JSON information for each url or infojson passed. Simulate unless --no-simulate is used. If the URL refers to a playlist, the whole playlist information is dumped in a single line
    DumpSingleJson,
    /// Force download archive entries to be written as far as no errors occur, even if -s or another simulation option is used (Alias: --force-download-archive)
    ForceWriteArchive,
    /// Output progress bar as new lines
    Newline,
    /// Do not print progress bar
    NoProgress,
    /// Show progress bar, even if in quiet mode
    Progress,
    /// Display progress in console titlebar
    ConsoleTitle,
    /// Template for progress outputs, optionally prefixed with one of "download:" (default), "download-title:" (the console title), "postprocess:",  or "postprocess-title:". The video's fields are accessible under the "info" key and the progress attributes are accessible under "progress" key. E.g.: --console-title --progress-template "download- title:%(info.id)s-%(progress.eta)s"
    ProgressTemplate(String),
    /// Print various debugging information
    Verbose,
    /// Print downloaded pages encoded using base64 to debug problems (very verbose)
    DumpPages,
    /// Write downloaded intermediary pages to files in the current directory to debug problems
    WritePages,
    /// Display sent and read HTTP traffic
    PrintTraffic,
    /// Force the specified encoding (experimental)
    Encoding(String),
    /// Explicitly allow HTTPS connection to servers that do not support RFC 5746 secure renegotiation
    LegacyServerConnect,
    /// Suppress HTTPS certificate validation
    NoCheckCertificates,
    /// Use an unencrypted connection to retrieve information about the video (Currently supported only for YouTube)
    PreferInsecure,
    /// Specify a custom HTTP header and its value, separated by a colon ":". You can use this option multiple times
    AddHeader(String),
    /// Work around terminals that lack bidirectional text support. Requires bidiv or fribidi executable in PATH
    BidiWorkaround,
    /// Number of seconds to sleep between requests during data extraction
    SleepRequests(String),
    /// Number of seconds to sleep before each download. This is the minimum time to sleep when used along with --max-sleep-interval (Alias: --min-sleep-interval)
    SleepInterval(String),
    /// Maximum number of seconds to sleep. Can only be used along with --min-sleep- interval
    MaxSleepInterval(String),
    /// Number of seconds to sleep before each subtitle download
    SleepSubtitles(String),
    /// Video format code, see "FORMAT SELECTION" for more details
    Format(String),
    /// Sort the formats by the fields given, see "Sorting Formats" for more details
    FormatSort(String),
    /// Force user specified sort order to have precedence over all fields, see "Sorting Formats" for more details
    FormatSortForce,
    /// Some fields have precedence over the user specified sort order (default), see "Sorting Formats" for more details
    NoFormatSortForce,
    /// Allow multiple video streams to be merged into a single file
    VideoMultistreams,
    /// Only one video stream is downloaded for each output file (default)
    NoVideoMultistreams,
    /// Allow multiple audio streams to be merged into a single file
    AudioMultistreams,
    /// Only one audio stream is downloaded for each output file (default)
    NoAudioMultistreams,
    /// Prefer video formats with free containers over non-free ones of same quality. Use with "-S ext" to strictly prefer free containers irrespective of quality
    PreferFreeFormats,
    /// Don't give any special preference to free containers (default)
    NoPreferFreeFormats,
    /// Check that the selected formats are actually downloadable
    CheckFormats,
    /// Check all formats for whether they are actually downloadable
    CheckAllFormats,
    /// Do not check that the formats are actually downloadable
    NoCheckFormats,
    /// List available formats of each video. Simulate unless --no-simulate is used
    ListFormats,
    /// If a merge is required (e.g. bestvideo+bestaudio), output to given container format. One of mkv, mp4, ogg, webm, flv. Ignored if no merge is required
    MergeOutputFormat(String),
    /// Write subtitle file
    WriteSubs,
    /// Do not write subtitle file (default)
    NoWriteSubs,
    /// Write automatically generated subtitle file (Alias: --write-automatic-subs)
    WriteAutoSubs,
    /// Do not write auto-generated subtitles (default) (Alias: --no-write-automatic- subs)
    NoWriteAutoSubs,
    /// List available subtitles of each video. Simulate unless --no-simulate is used
    ListSubs,
    /// Subtitle format, accepts formats preference, for example: "srt" or "ass/srt/best"
    SubFormat(String),
    /// Languages of the subtitles to download (can be regex) or "all" separated by commas. (Eg: --sub-langs "en.*,ja") You can prefix the language code with a "-" to exempt it from the requested languages. (Eg: --sub- langs all,-live_chat) Use --list-subs for a list of available language tags
    SubLangs(String),
    /// Login with this account ID
    Username(String),
    /// Account password. If this option is left out, yt-dlp will ask interactively
    Password(String),
    /// Two-factor authentication code
    Twofactor(String),
    /// Use .netrc authentication data
    Netrc,
    /// Location of .netrc authentication data; either the path or its containing directory. Defaults to ~/.netrc
    NetrcLocation(String),
    /// Video password (vimeo, youku)
    VideoPassword(String),
    /// Adobe Pass multiple-system operator (TV provider) identifier, use --ap-list-mso for a list of available MSOs
    ApMso(String),
    /// Multiple-system operator account login
    ApUsername(String),
    /// Multiple-system operator account password. If this option is left out, yt-dlp will ask interactively
    ApPassword(String),
    /// List all supported multiple-system operators
    ApListMso,
    /// Convert video files to audio-only files (requires ffmpeg and ffprobe)
    ExtractAudio,
    /// Specify audio format to convert the audio to when -x is used. Currently supported formats are: best (default) or one of aac|flac|mp3|m4a|opus|vorbis|wav|alac
    AudioFormat(String),
    /// Specify ffmpeg audio quality, insert a value between 0 (best) and 10 (worst) for VBR or a specific bitrate like 128K (default 5)
    AudioQuality(String),
    /// Remux the video into another container if necessary (currently supported: mp4|mkv|flv |webm|mov|avi|mp3|mka|m4a|ogg|opus). If target container does not support the video/audio codec, remuxing will fail. You can specify multiple rules; Eg. "aac>m4a/mov>mp4/mkv" will remux aac to m4a, mov to mp4 and anything else to mkv.
    RemuxVideo(String),
    /// Re-encode the video into another format if re-encoding is necessary. The syntax and supported formats are the same as --remux- video
    RecodeVideo(String),
    /// Give these arguments to the postprocessors. Specify the postprocessor/executable name and the arguments separated by a colon ":" to give the argument to the specified postprocessor/executable. Supported PP are: Merger, ModifyChapters, SplitChapters, ExtractAudio, VideoRemuxer, VideoConvertor, Metadata, EmbedSubtitle, EmbedThumbnail, SubtitlesConvertor, ThumbnailsConvertor, FixupStretched, FixupM4a, FixupM3u8, FixupTimestamp and FixupDuration. The supported executables are: AtomicParsley, FFmpeg and FFprobe. You can also specify "PP+EXE:ARGS" to give the arguments to the specified executable only when being used by the specified postprocessor. Additionally, for ffmpeg/ffprobe, "_i"/"_o" can be appended to the prefix optionally followed by a number to pass the argument before the specified input/output file. Eg: --ppa "Merger+ffmpeg_i1:-v quiet". You can use this option multiple times to give different arguments to different postprocessors. (Alias: --ppa)
    PostprocessorArgs(String),
    /// Keep the intermediate video file on disk after post-processing
    KeepVideo,
    /// Delete the intermediate video file after post-processing (default)
    NoKeepVideo,
    /// Overwrite post-processed files (default)
    PostOverwrites,
    /// Do not overwrite post-processed files
    NoPostOverwrites,
    /// Embed subtitles in the video (only for mp4, webm and mkv videos)
    EmbedSubs,
    /// Do not embed subtitles (default)
    NoEmbedSubs,
    /// Embed thumbnail in the video as cover art
    EmbedThumbnail,
    /// Do not embed thumbnail (default)
    NoEmbedThumbnail,
    /// Embed metadata to the video file. Also embeds chapters/infojson if present unless --no-embed-chapters/--no-embed-info-json are used (Alias: --add-metadata)
    EmbedMetadata,
    /// Do not add metadata to file (default) (Alias: --no-add-metadata)
    NoEmbedMetadata,
    /// Add chapter markers to the video file (Alias: --add-chapters)
    EmbedChapters,
    /// Do not add chapter markers (default) (Alias: --no-add-chapters)
    NoEmbedChapters,
    /// Embed the infojson as an attachment to mkv/mka video files
    EmbedInfoJson,
    /// Do not embed the infojson as an attachment to the video file
    NoEmbedInfoJson,
    /// Parse additional metadata like title/artist from other fields; see "MODIFYING METADATA" for details
    ParseMetadata(String),
    /// Replace text in a metadata field using the given regex. This option can be used multiple times
    ReplaceInMetadata(String, String, String),
    /// Write metadata to the video file's xattrs (using dublin core and xdg standards)
    Xattrs,
    /// Concatenate videos in a playlist. One of "never", "always", or "multi_video" (default; only when the videos form a single show). All the video files must have same codecs and number of streams to be concatable. The "pl_video:" prefix can be used with "--paths" and "--output" to set the output filename for the split files. See "OUTPUT TEMPLATE" for details
    ConcatPlaylist(String),
    /// Automatically correct known faults of the file. One of never (do nothing), warn (only emit a warning), detect_or_warn (the default; fix file if we can, warn otherwise), force (try fixing even if file already exists)
    Fixup(String),
    /// Location of the ffmpeg binary; either the path to the binary or its containing directory
    FfmpegLocation(String),
    /// Execute a command, optionally prefixed with when to execute it (after_move if unspecified), separated by a ":". Supported values of "WHEN" are the same as that of --use-postprocessor. Same syntax as the output template can be used to pass any field as arguments to the command. After download, an additional field "filepath" that contains the final path of the downloaded file is also available, and if no fields are passed, %(filepath)q is appended to the end of the command. This option can be used multiple times
    Exec(String),
    /// Remove any previously defined --exec
    NoExec,
    /// Convert the subtitles to another format (currently supported: srt|vtt|ass|lrc) (Alias: --convert-subtitles)
    ConvertSubs(String),
    /// Convert the thumbnails to another format (currently supported: jpg|png|webp)
    ConvertThumbnails(String),
    /// Split video into multiple files based on internal chapters. The "chapter:" prefix can be used with "--paths" and "--output" to set the output filename for the split files. See "OUTPUT TEMPLATE" for details
    SplitChapters,
    /// Do not split video based on chapters (default)
    NoSplitChapters,
    /// Remove chapters whose title matches the given regular expression. Time ranges prefixed by a "*" can also be used in place of chapters to remove the specified range. Eg: --remove-chapters "*10:15-15:00" --remove-chapters "intro". This option can be used multiple times
    RemoveChapters(String),
    /// Do not remove any chapters from the file (default)
    NoRemoveChapters,
    /// Force keyframes around the chapters before removing/splitting them. Requires a re- encode and thus is very slow, but the resulting video may have fewer artifacts around the cuts
    ForceKeyframesAtCuts,
    /// Do not force keyframes around the chapters when cutting/splitting (default)
    NoForceKeyframesAtCuts,
    /// The (case sensitive) name of plugin postprocessors to be enabled, and (optionally) arguments to be passed to it, separated by a colon ":". ARGS are a semicolon ";" delimited list of NAME=VALUE. The "when" argument determines when the postprocessor is invoked. It can be one of "pre_process" (after video extraction), "after_filter" (after video passes filter), "before_dl" (before each video download), "post_process" (after each video download; default), "after_move" (after moving video file to it's final locations), "after_video" (after downloading and processing all formats of a video), or "playlist" (at end of playlist). This option can be used multiple times to add different postprocessors
    UsePostprocessor(String),
    /// SponsorBlock categories to create chapters for, separated by commas. Available categories are all, default(=all), sponsor, intro, outro, selfpromo, preview, filler, interaction, music_offtopic, poi_highlight. You can prefix the category with a "-" to exempt it. See [1] for description of the categories. Eg: --sponsorblock-mark all,-preview [1] https://wiki.sponsor.ajay. app/w/Segment_Categories
    SponsorblockMark(String),
    /// SponsorBlock categories to be removed from the video file, separated by commas. If a category is present in both mark and remove, remove takes precedence. The syntax and available categories are the same as for --sponsorblock-mark except that "default" refers to "all,-filler" and poi_highlight is not available
    SponsorblockRemove(String),
    /// The title template for SponsorBlock chapters created by --sponsorblock-mark. The same syntax as the output template is used, but the only available fields are start_time, end_time, category, categories, name, category_names. Defaults to "[SponsorBlock]: %(category_names)l"
    SponsorblockChapterTitle(String),
    /// Disable both --sponsorblock-mark and --sponsorblock-remove
    NoSponsorblock,
    /// SponsorBlock API location, defaults to https://sponsor.ajay.app
    SponsorblockApi(String),
    /// Number of retries for known extractor errors (default is 3), or "infinite"
    ExtractorRetries(String),
    /// Process dynamic DASH manifests (default) (Alias: --no-ignore-dynamic-mpd)
    AllowDynamicMpd,
    /// Do not process dynamic DASH manifests (Alias: --no-allow-dynamic-mpd)
    IgnoreDynamicMpd,
    /// Split HLS playlists to different formats at discontinuities such as ad breaks
    HlsSplitDiscontinuity,
    /// Do not split HLS playlists to different formats at discontinuities such as ad breaks (default)
    NoHlsSplitDiscontinuity,
    /// Pass these arguments to the extractor. See "EXTRACTOR ARGUMENTS" for details. You can use this option multiple times to give arguments for different extractors
    ExtractorArgs(String),
    /// video url
    Url(String),
}

impl YtDlpOption {
    pub fn to_args(o: YtDlpOption) -> Vec<String> {
        use YtDlpOption::*;
        match o {
            Help => vec!["-h".to_string()],
            Version => vec!["--version".to_string()],
            Update => vec!["-U".to_string()],
            IgnoreErrors => vec!["-i".to_string()],
            NoAbortOnError => vec!["--no-abort-on-error".to_string()],
            AbortOnError => vec!["--abort-on-error".to_string()],
            DumpUserAgent => vec!["--dump-user-agent".to_string()],
            ListExtractors => vec!["--list-extractors".to_string()],
            ExtractorDescriptions => vec!["--extractor-descriptions".to_string()],
            ForceGenericExtractor => vec!["--force-generic-extractor".to_string()],
            DefaultSearch(v0) => vec!["--default-search".to_string(), v0],
            IgnoreConfig => vec!["--ignore-config".to_string()],
            NoConfigLocations => vec!["--no-config-locations".to_string()],
            ConfigLocations(v0) => vec!["--config-locations".to_string(), v0],
            FlatPlaylist => vec!["--flat-playlist".to_string()],
            NoFlatPlaylist => vec!["--no-flat-playlist".to_string()],
            LiveFromStart => vec!["--live-from-start".to_string()],
            NoLiveFromStart => vec!["--no-live-from-start".to_string()],
            WaitForVideo => vec!["--wait-for-video".to_string()],
            NoWaitForVideo => vec!["--no-wait-for-video".to_string()],
            MarkWatched => vec!["--mark-watched".to_string()],
            NoMarkWatched => vec!["--no-mark-watched".to_string()],
            NoColors => vec!["--no-colors".to_string()],
            CompatOptions(v0) => vec!["--compat-options".to_string(), v0],
            Proxy(v0) => vec!["--proxy".to_string(), v0],
            SocketTimeout(v0) => vec!["--socket-timeout".to_string(), v0],
            SourceAddress(v0) => vec!["--source-address".to_string(), v0],
            ForceIpv4 => vec!["-4".to_string()],
            ForceIpv6 => vec!["-6".to_string()],
            GeoVerificationProxy(v0) => vec!["--geo-verification-proxy".to_string(), v0],
            GeoBypass => vec!["--geo-bypass".to_string()],
            NoGeoBypass => vec!["--no-geo-bypass".to_string()],
            GeoBypassCountry(v0) => vec!["--geo-bypass-country".to_string(), v0],
            GeoBypassIpBlock => vec!["--geo-bypass-ip-block".to_string()],
            PlaylistStart(v0) => vec!["--playlist-start".to_string(), v0],
            PlaylistEnd(v0) => vec!["--playlist-end".to_string(), v0],
            PlaylistItems => vec!["--playlist-items".to_string()],
            MinFilesize(v0) => vec!["--min-filesize".to_string(), v0],
            MaxFilesize(v0) => vec!["--max-filesize".to_string(), v0],
            Date(v0) => vec!["--date".to_string(), v0],
            Datebefore(v0) => vec!["--datebefore".to_string(), v0],
            Dateafter(v0) => vec!["--dateafter".to_string(), v0],
            MatchFilter(v0) => vec!["--match-filter".to_string(), v0],
            NoMatchFilter => vec!["--no-match-filter".to_string()],
            NoPlaylist => vec!["--no-playlist".to_string()],
            YesPlaylist => vec!["--yes-playlist".to_string()],
            AgeLimit(v0) => vec!["--age-limit".to_string(), v0],
            DownloadArchive(v0) => vec!["--download-archive".to_string(), v0],
            NoDownloadArchive => vec!["--no-download-archive".to_string()],
            MaxDownloads(v0) => vec!["--max-downloads".to_string(), v0],
            BreakOnExisting => vec!["--break-on-existing".to_string()],
            BreakOnReject => vec!["--break-on-reject".to_string()],
            BreakPerInput => vec!["--break-per-input".to_string()],
            NoBreakPerInput => vec!["--no-break-per-input".to_string()],
            SkipPlaylistAfterErrors(v0) => vec!["--skip-playlist-after-errors".to_string(), v0],
            ConcurrentFragments(v0) => vec!["-N".to_string(), v0],
            LimitRate(v0) => vec!["-r".to_string(), v0],
            ThrottledRate(v0) => vec!["--throttled-rate".to_string(), v0],
            Retries(v0) => vec!["-R".to_string(), v0],
            FileAccessRetries(v0) => vec!["--file-access-retries".to_string(), v0],
            FragmentRetries(v0) => vec!["--fragment-retries".to_string(), v0],
            SkipUnavailableFragments => vec!["--skip-unavailable-fragments".to_string()],
            AbortOnUnavailableFragment => vec!["--abort-on-unavailable-fragment".to_string()],
            KeepFragments => vec!["--keep-fragments".to_string()],
            NoKeepFragments => vec!["--no-keep-fragments".to_string()],
            BufferSize(v0) => vec!["--buffer-size".to_string(), v0],
            ResizeBuffer => vec!["--resize-buffer".to_string()],
            NoResizeBuffer => vec!["--no-resize-buffer".to_string()],
            HttpChunkSize(v0) => vec!["--http-chunk-size".to_string(), v0],
            PlaylistReverse => vec!["--playlist-reverse".to_string()],
            NoPlaylistReverse => vec!["--no-playlist-reverse".to_string()],
            PlaylistRandom => vec!["--playlist-random".to_string()],
            XattrSetFilesize => vec!["--xattr-set-filesize".to_string()],
            HlsUseMpegts => vec!["--hls-use-mpegts".to_string()],
            NoHlsUseMpegts => vec!["--no-hls-use-mpegts".to_string()],
            Downloader(v0) => vec!["--downloader".to_string(), v0],
            DownloaderArgs(v0) => vec!["--downloader-args".to_string(), v0],
            BatchFile(v0) => vec!["-a".to_string(), v0],
            NoBatchFile => vec!["--no-batch-file".to_string()],
            Paths(v0) => vec!["-P".to_string(), v0],
            Output(v0) => vec!["-o".to_string(), v0],
            OutputNaPlaceholder(v0) => vec!["--output-na-placeholder".to_string(), v0],
            RestrictFilenames => vec!["--restrict-filenames".to_string()],
            NoRestrictFilenames => vec!["--no-restrict-filenames".to_string()],
            WindowsFilenames => vec!["--windows-filenames".to_string()],
            NoWindowsFilenames => vec!["--no-windows-filenames".to_string()],
            TrimFilenames(v0) => vec!["--trim-filenames".to_string(), v0],
            NoOverwrites => vec!["-w".to_string()],
            ForceOverwrites => vec!["--force-overwrites".to_string()],
            NoForceOverwrites => vec!["--no-force-overwrites".to_string()],
            Continue => vec!["-c".to_string()],
            NoContinue => vec!["--no-continue".to_string()],
            Part => vec!["--part".to_string()],
            NoPart => vec!["--no-part".to_string()],
            Mtime => vec!["--mtime".to_string()],
            NoMtime => vec!["--no-mtime".to_string()],
            WriteDescription => vec!["--write-description".to_string()],
            NoWriteDescription => vec!["--no-write-description".to_string()],
            WriteInfoJson => vec!["--write-info-json".to_string()],
            NoWriteInfoJson => vec!["--no-write-info-json".to_string()],
            WritePlaylistMetafiles => vec!["--write-playlist-metafiles".to_string()],
            NoWritePlaylistMetafiles => vec!["--no-write-playlist-metafiles".to_string()],
            CleanInfoJson => vec!["--clean-info-json".to_string()],
            NoCleanInfoJson => vec!["--no-clean-info-json".to_string()],
            WriteComments => vec!["--write-comments".to_string()],
            NoWriteComments => vec!["--no-write-comments".to_string()],
            LoadInfoJson(v0) => vec!["--load-info-json".to_string(), v0],
            Cookies(v0) => vec!["--cookies".to_string(), v0],
            NoCookies => vec!["--no-cookies".to_string()],
            CookiesFromBrowser => vec!["--cookies-from-browser".to_string()],
            NoCookiesFromBrowser => vec!["--no-cookies-from-browser".to_string()],
            CacheDir(v0) => vec!["--cache-dir".to_string(), v0],
            NoCacheDir => vec!["--no-cache-dir".to_string()],
            RmCacheDir => vec!["--rm-cache-dir".to_string()],
            WriteThumbnail => vec!["--write-thumbnail".to_string()],
            NoWriteThumbnail => vec!["--no-write-thumbnail".to_string()],
            WriteAllThumbnails => vec!["--write-all-thumbnails".to_string()],
            ListThumbnails => vec!["--list-thumbnails".to_string()],
            WriteLink => vec!["--write-link".to_string()],
            WriteUrlLink => vec!["--write-url-link".to_string()],
            WriteWeblocLink => vec!["--write-webloc-link".to_string()],
            WriteDesktopLink => vec!["--write-desktop-link".to_string()],
            Quiet => vec!["-q".to_string()],
            NoWarnings => vec!["--no-warnings".to_string()],
            Simulate => vec!["-s".to_string()],
            NoSimulate => vec!["--no-simulate".to_string()],
            IgnoreNoFormatsError => vec!["--ignore-no-formats-error".to_string()],
            NoIgnoreNoFormatsError => vec!["--no-ignore-no-formats-error".to_string()],
            SkipDownload => vec!["--skip-download".to_string()],
            Print(v0) => vec!["-O".to_string(), v0],
            PrintToFile(v0, v1) => vec!["--print-to-file".to_string(), v0, v1],
            DumpJson => vec!["-j".to_string()],
            DumpSingleJson => vec!["-J".to_string()],
            ForceWriteArchive => vec!["--force-write-archive".to_string()],
            Newline => vec!["--newline".to_string()],
            NoProgress => vec!["--no-progress".to_string()],
            Progress => vec!["--progress".to_string()],
            ConsoleTitle => vec!["--console-title".to_string()],
            ProgressTemplate(v0) => vec!["--progress-template".to_string(), v0],
            Verbose => vec!["-v".to_string()],
            DumpPages => vec!["--dump-pages".to_string()],
            WritePages => vec!["--write-pages".to_string()],
            PrintTraffic => vec!["--print-traffic".to_string()],
            Encoding(v0) => vec!["--encoding".to_string(), v0],
            LegacyServerConnect => vec!["--legacy-server-connect".to_string()],
            NoCheckCertificates => vec!["--no-check-certificates".to_string()],
            PreferInsecure => vec!["--prefer-insecure".to_string()],
            AddHeader(v0) => vec!["--add-header".to_string(), v0],
            BidiWorkaround => vec!["--bidi-workaround".to_string()],
            SleepRequests(v0) => vec!["--sleep-requests".to_string(), v0],
            SleepInterval(v0) => vec!["--sleep-interval".to_string(), v0],
            MaxSleepInterval(v0) => vec!["--max-sleep-interval".to_string(), v0],
            SleepSubtitles(v0) => vec!["--sleep-subtitles".to_string(), v0],
            Format(v0) => vec!["-f".to_string(), v0],
            FormatSort(v0) => vec!["-S".to_string(), v0],
            FormatSortForce => vec!["--format-sort-force".to_string()],
            NoFormatSortForce => vec!["--no-format-sort-force".to_string()],
            VideoMultistreams => vec!["--video-multistreams".to_string()],
            NoVideoMultistreams => vec!["--no-video-multistreams".to_string()],
            AudioMultistreams => vec!["--audio-multistreams".to_string()],
            NoAudioMultistreams => vec!["--no-audio-multistreams".to_string()],
            PreferFreeFormats => vec!["--prefer-free-formats".to_string()],
            NoPreferFreeFormats => vec!["--no-prefer-free-formats".to_string()],
            CheckFormats => vec!["--check-formats".to_string()],
            CheckAllFormats => vec!["--check-all-formats".to_string()],
            NoCheckFormats => vec!["--no-check-formats".to_string()],
            ListFormats => vec!["-F".to_string()],
            MergeOutputFormat(v0) => vec!["--merge-output-format".to_string(), v0],
            WriteSubs => vec!["--write-subs".to_string()],
            NoWriteSubs => vec!["--no-write-subs".to_string()],
            WriteAutoSubs => vec!["--write-auto-subs".to_string()],
            NoWriteAutoSubs => vec!["--no-write-auto-subs".to_string()],
            ListSubs => vec!["--list-subs".to_string()],
            SubFormat(v0) => vec!["--sub-format".to_string(), v0],
            SubLangs(v0) => vec!["--sub-langs".to_string(), v0],
            Username(v0) => vec!["-u".to_string(), v0],
            Password(v0) => vec!["-p".to_string(), v0],
            Twofactor(v0) => vec!["-2".to_string(), v0],
            Netrc => vec!["-n".to_string()],
            NetrcLocation(v0) => vec!["--netrc-location".to_string(), v0],
            VideoPassword(v0) => vec!["--video-password".to_string(), v0],
            ApMso(v0) => vec!["--ap-mso".to_string(), v0],
            ApUsername(v0) => vec!["--ap-username".to_string(), v0],
            ApPassword(v0) => vec!["--ap-password".to_string(), v0],
            ApListMso => vec!["--ap-list-mso".to_string()],
            ExtractAudio => vec!["-x".to_string()],
            AudioFormat(v0) => vec!["--audio-format".to_string(), v0],
            AudioQuality(v0) => vec!["--audio-quality".to_string(), v0],
            RemuxVideo(v0) => vec!["--remux-video".to_string(), v0],
            RecodeVideo(v0) => vec!["--recode-video".to_string(), v0],
            PostprocessorArgs(v0) => vec!["--postprocessor-args".to_string(), v0],
            KeepVideo => vec!["-k".to_string()],
            NoKeepVideo => vec!["--no-keep-video".to_string()],
            PostOverwrites => vec!["--post-overwrites".to_string()],
            NoPostOverwrites => vec!["--no-post-overwrites".to_string()],
            EmbedSubs => vec!["--embed-subs".to_string()],
            NoEmbedSubs => vec!["--no-embed-subs".to_string()],
            EmbedThumbnail => vec!["--embed-thumbnail".to_string()],
            NoEmbedThumbnail => vec!["--no-embed-thumbnail".to_string()],
            EmbedMetadata => vec!["--embed-metadata".to_string()],
            NoEmbedMetadata => vec!["--no-embed-metadata".to_string()],
            EmbedChapters => vec!["--embed-chapters".to_string()],
            NoEmbedChapters => vec!["--no-embed-chapters".to_string()],
            EmbedInfoJson => vec!["--embed-info-json".to_string()],
            NoEmbedInfoJson => vec!["--no-embed-info-json".to_string()],
            ParseMetadata(v0) => vec!["--parse-metadata".to_string(), v0],
            ReplaceInMetadata(v0, v1, v2) => vec!["--replace-in-metadata".to_string(), v0, v1, v2],
            Xattrs => vec!["--xattrs".to_string()],
            ConcatPlaylist(v0) => vec!["--concat-playlist".to_string(), v0],
            Fixup(v0) => vec!["--fixup".to_string(), v0],
            FfmpegLocation(v0) => vec!["--ffmpeg-location".to_string(), v0],
            Exec(v0) => vec!["--exec".to_string(), v0],
            NoExec => vec!["--no-exec".to_string()],
            ConvertSubs(v0) => vec!["--convert-subs".to_string(), v0],
            ConvertThumbnails(v0) => vec!["--convert-thumbnails".to_string(), v0],
            SplitChapters => vec!["--split-chapters".to_string()],
            NoSplitChapters => vec!["--no-split-chapters".to_string()],
            RemoveChapters(v0) => vec!["--remove-chapters".to_string(), v0],
            NoRemoveChapters => vec!["--no-remove-chapters".to_string()],
            ForceKeyframesAtCuts => vec!["--force-keyframes-at-cuts".to_string()],
            NoForceKeyframesAtCuts => vec!["--no-force-keyframes-at-cuts".to_string()],
            UsePostprocessor(v0) => vec!["--use-postprocessor".to_string(), v0],
            SponsorblockMark(v0) => vec!["--sponsorblock-mark".to_string(), v0],
            SponsorblockRemove(v0) => vec!["--sponsorblock-remove".to_string(), v0],
            SponsorblockChapterTitle(v0) => vec!["--sponsorblock-chapter-title".to_string(), v0],
            NoSponsorblock => vec!["--no-sponsorblock".to_string()],
            SponsorblockApi(v0) => vec!["--sponsorblock-api".to_string(), v0],
            ExtractorRetries(v0) => vec!["--extractor-retries".to_string(), v0],
            AllowDynamicMpd => vec!["--allow-dynamic-mpd".to_string()],
            IgnoreDynamicMpd => vec!["--ignore-dynamic-mpd".to_string()],
            HlsSplitDiscontinuity => vec!["--hls-split-discontinuity".to_string()],
            NoHlsSplitDiscontinuity => vec!["--no-hls-split-discontinuity".to_string()],
            ExtractorArgs(v0) => vec!["--extractor-args".to_string(), v0],
            Url(v0) => vec!["--".to_string(), v0],
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct YtDlpProcess(Vec<YtDlpOption>, Option<std::time::Duration>);

impl YtDlpProcess {
    /// Print this help text and exit
    pub fn help(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Help);
        self
    }

    /// Print program version and exit
    pub fn version(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Version);
        self
    }

    /// Update this program to latest version. Make sure that you have sufficient permissions (run with sudo if needed)
    pub fn update(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Update);
        self
    }

    /// Ignore download and postprocessing errors. The download will be considered successful even if the postprocessing fails
    pub fn ignore_errors(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::IgnoreErrors);
        self
    }

    /// Continue with next video on download errors; e.g. to skip unavailable videos in a playlist (default)
    pub fn no_abort_on_error(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoAbortOnError);
        self
    }

    /// Abort downloading of further videos if an error occurs (Alias: --no-ignore-errors)
    pub fn abort_on_error(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::AbortOnError);
        self
    }

    /// Display the current user-agent and exit
    pub fn dump_user_agent(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::DumpUserAgent);
        self
    }

    /// List all supported extractors and exit
    pub fn list_extractors(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ListExtractors);
        self
    }

    /// Output descriptions of all supported extractors and exit
    pub fn extractor_descriptions(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ExtractorDescriptions);
        self
    }

    /// Force extraction to use the generic extractor
    pub fn force_generic_extractor(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceGenericExtractor);
        self
    }

    /// Use this prefix for unqualified URLs. For example "gvsearch2:" downloads two videos from google videos for the search term "large apple". Use the value "auto" to let yt-dlp guess ("auto_warning" to emit a warning when guessing). "error" just throws an error. The default value "fixup_error" repairs broken URLs, but emits an error if this is not possible instead of searching
    pub fn default_search(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::DefaultSearch(v.to_string()));
        self
    }

    /// Don't load any more configuration files except those given by --config-locations. For backward compatibility, if this option is found inside the system configuration file, the user configuration is not loaded. (Alias: --no-config)
    pub fn ignore_config(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::IgnoreConfig);
        self
    }

    /// Do not load any custom configuration files (default). When given inside a configuration file, ignore all previous --config-locations defined in the current file
    pub fn no_config_locations(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoConfigLocations);
        self
    }

    /// Location of the main configuration file; either the path to the config or its containing directory. Can be used multiple times and inside other configuration files
    pub fn config_locations(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ConfigLocations(v.to_string()));
        self
    }

    /// Do not extract the videos of a playlist, only list them
    pub fn flat_playlist(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::FlatPlaylist);
        self
    }

    /// Extract the videos of a playlist
    pub fn no_flat_playlist(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoFlatPlaylist);
        self
    }

    /// Download livestreams from the start. Currently only supported for YouTube (Experimental)
    pub fn live_from_start(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::LiveFromStart);
        self
    }

    /// Download livestreams from the current time (default)
    pub fn no_live_from_start(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoLiveFromStart);
        self
    }

    /// MIN[-MAX]       Wait for scheduled streams to become available. Pass the minimum number of seconds (or range) to wait between retries
    pub fn wait_for_video(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WaitForVideo);
        self
    }

    /// Do not wait for scheduled streams (default)
    pub fn no_wait_for_video(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWaitForVideo);
        self
    }

    /// Mark videos watched (even with --simulate)
    pub fn mark_watched(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::MarkWatched);
        self
    }

    /// Do not mark videos watched (default)
    pub fn no_mark_watched(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoMarkWatched);
        self
    }

    /// Do not emit color codes in output
    pub fn no_colors(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoColors);
        self
    }

    /// Options that can help keep compatibility with youtube-dl or youtube-dlc configurations by reverting some of the changes made in yt-dlp. See "Differences in default behavior" for details
    pub fn compat_options(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::CompatOptions(v.to_string()));
        self
    }

    /// Use the specified HTTP/HTTPS/SOCKS proxy. To enable SOCKS proxy, specify a proper scheme. For example socks5://user:pass@127.0.0.1:1080/. Pass in an empty string (--proxy "") for direct connection
    pub fn proxy(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Proxy(v.to_string()));
        self
    }

    /// Time to wait before giving up, in seconds
    pub fn socket_timeout(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SocketTimeout(v.to_string()));
        self
    }

    /// Client-side IP address to bind to
    pub fn source_address(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SourceAddress(v.to_string()));
        self
    }

    /// Make all connections via IPv4
    pub fn force_ipv4(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceIpv4);
        self
    }

    /// Make all connections via IPv6
    pub fn force_ipv6(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceIpv6);
        self
    }

    /// Use this proxy to verify the IP address for some geo-restricted sites. The default proxy specified by --proxy (or none, if the option is not present) is used for the actual downloading
    pub fn geo_verification_proxy(&mut self, v: &str) -> &mut Self {
        self.0
            .push(YtDlpOption::GeoVerificationProxy(v.to_string()));
        self
    }

    /// Bypass geographic restriction via faking X-Forwarded-For HTTP header (default)
    pub fn geo_bypass(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::GeoBypass);
        self
    }

    /// Do not bypass geographic restriction via faking X-Forwarded-For HTTP header
    pub fn no_geo_bypass(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoGeoBypass);
        self
    }

    /// Force bypass geographic restriction with explicitly provided two-letter ISO 3166-2 country code
    pub fn geo_bypass_country(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::GeoBypassCountry(v.to_string()));
        self
    }

    /// IP_BLOCK   Force bypass geographic restriction with explicitly provided IP block in CIDR notation
    pub fn geo_bypass_ip_block(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::GeoBypassIpBlock);
        self
    }

    /// Playlist video to start at (default is 1)
    pub fn playlist_start(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::PlaylistStart(v.to_string()));
        self
    }

    /// Playlist video to end at (default is last)
    pub fn playlist_end(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::PlaylistEnd(v.to_string()));
        self
    }

    /// ITEM_SPEC       Playlist video items to download. Specify indices of the videos in the playlist separated by commas like: "--playlist-items 1,2,5,8" if you want to download videos indexed 1, 2, 5, 8 in the playlist. You can specify range: "--playlist-items 1-3,7,10-13", it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13
    pub fn playlist_items(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PlaylistItems);
        self
    }

    /// Do not download any videos smaller than SIZE (e.g. 50k or 44.6m)
    pub fn min_filesize(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MinFilesize(v.to_string()));
        self
    }

    /// Do not download any videos larger than SIZE (e.g. 50k or 44.6m)
    pub fn max_filesize(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MaxFilesize(v.to_string()));
        self
    }

    /// Download only videos uploaded on this date. The date can be "YYYYMMDD" or in the format "(now|today)[+-][0- 9](day|week|month|year)(s)?"
    pub fn date(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Date(v.to_string()));
        self
    }

    /// Download only videos uploaded on or before this date. The date formats accepted is the same as --date
    pub fn datebefore(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Datebefore(v.to_string()));
        self
    }

    /// Download only videos uploaded on or after this date. The date formats accepted is the same as --date
    pub fn dateafter(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Dateafter(v.to_string()));
        self
    }

    /// Generic video filter. Any field (see "OUTPUT TEMPLATE") can be compared with a number or a string using the operators defined in "Filtering formats". You can also simply specify a field to match if the field is present and "!field" to check if the field is not present. In addition, Python style regular expression matching can be done using "~=", and multiple filters can be checked with "&". Use a "\" to escape "&" or quotes if needed. Eg: --match-filter "!is_live & like_count>?100 & description~='(?i)\bcats \& dogs\b'" matches only videos that are not live, has a like count more than 100 (or the like field is not available), and also has a description that contains the phrase "cats & dogs" (ignoring case)
    pub fn match_filter(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MatchFilter(v.to_string()));
        self
    }

    /// Do not use generic video filter (default)
    pub fn no_match_filter(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoMatchFilter);
        self
    }

    /// Download only the video, if the URL refers to a video and a playlist
    pub fn no_playlist(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoPlaylist);
        self
    }

    /// Download the playlist, if the URL refers to a video and a playlist
    pub fn yes_playlist(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::YesPlaylist);
        self
    }

    /// Download only videos suitable for the given age
    pub fn age_limit(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::AgeLimit(v.to_string()));
        self
    }

    /// Download only videos not listed in the archive file. Record the IDs of all downloaded videos in it
    pub fn download_archive(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::DownloadArchive(v.to_string()));
        self
    }

    /// Do not use archive file (default)
    pub fn no_download_archive(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoDownloadArchive);
        self
    }

    /// Abort after downloading NUMBER files
    pub fn max_downloads(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MaxDownloads(v.to_string()));
        self
    }

    /// Stop the download process when encountering a file that is in the archive
    pub fn break_on_existing(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::BreakOnExisting);
        self
    }

    /// Stop the download process when encountering a file that has been filtered out
    pub fn break_on_reject(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::BreakOnReject);
        self
    }

    /// Make --break-on-existing and --break-on- reject act only on the current input URL
    pub fn break_per_input(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::BreakPerInput);
        self
    }

    /// --break-on-existing and --break-on-reject terminates the entire download queue
    pub fn no_break_per_input(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoBreakPerInput);
        self
    }

    /// Number of allowed failures until the rest of the playlist is skipped
    pub fn skip_playlist_after_errors(&mut self, v: &str) -> &mut Self {
        self.0
            .push(YtDlpOption::SkipPlaylistAfterErrors(v.to_string()));
        self
    }

    /// Number of fragments of a dash/hlsnative video that should be downloaded concurrently (default is 1)
    pub fn concurrent_fragments(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ConcurrentFragments(v.to_string()));
        self
    }

    /// Maximum download rate in bytes per second (e.g. 50K or 4.2M)
    pub fn limit_rate(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::LimitRate(v.to_string()));
        self
    }

    /// Minimum download rate in bytes per second below which throttling is assumed and the video data is re-extracted (e.g. 100K)
    pub fn throttled_rate(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ThrottledRate(v.to_string()));
        self
    }

    /// Number of retries (default is 10), or "infinite"
    pub fn retries(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Retries(v.to_string()));
        self
    }

    /// Number of times to retry on file access error (default is 3), or "infinite"
    pub fn file_access_retries(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::FileAccessRetries(v.to_string()));
        self
    }

    /// Number of retries for a fragment (default is 10), or "infinite" (DASH, hlsnative and ISM)
    pub fn fragment_retries(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::FragmentRetries(v.to_string()));
        self
    }

    /// Skip unavailable fragments for DASH, hlsnative and ISM (default) (Alias: --no- abort-on-unavailable-fragment)
    pub fn skip_unavailable_fragments(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::SkipUnavailableFragments);
        self
    }

    /// Abort downloading if a fragment is unavailable (Alias: --no-skip-unavailable- fragments)
    pub fn abort_on_unavailable_fragment(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::AbortOnUnavailableFragment);
        self
    }

    /// Keep downloaded fragments on disk after downloading is finished
    pub fn keep_fragments(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::KeepFragments);
        self
    }

    /// Delete downloaded fragments after downloading is finished (default)
    pub fn no_keep_fragments(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoKeepFragments);
        self
    }

    /// Size of download buffer (e.g. 1024 or 16K) (default is 1024)
    pub fn buffer_size(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::BufferSize(v.to_string()));
        self
    }

    /// The buffer size is automatically resized from an initial value of --buffer-size (default)
    pub fn resize_buffer(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ResizeBuffer);
        self
    }

    /// Do not automatically adjust the buffer size
    pub fn no_resize_buffer(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoResizeBuffer);
        self
    }

    /// Size of a chunk for chunk-based HTTP downloading (e.g. 10485760 or 10M) (default is disabled). May be useful for bypassing bandwidth throttling imposed by a webserver (experimental)
    pub fn http_chunk_size(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::HttpChunkSize(v.to_string()));
        self
    }

    /// Download playlist videos in reverse order
    pub fn playlist_reverse(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PlaylistReverse);
        self
    }

    /// Download playlist videos in default order (default)
    pub fn no_playlist_reverse(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoPlaylistReverse);
        self
    }

    /// Download playlist videos in random order
    pub fn playlist_random(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PlaylistRandom);
        self
    }

    /// Set file xattribute ytdl.filesize with expected file size
    pub fn xattr_set_filesize(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::XattrSetFilesize);
        self
    }

    /// Use the mpegts container for HLS videos; allowing some players to play the video while downloading, and reducing the chance of file corruption if download is interrupted. This is enabled by default for live streams
    pub fn hls_use_mpegts(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::HlsUseMpegts);
        self
    }

    /// Do not use the mpegts container for HLS videos. This is default when not downloading live streams
    pub fn no_hls_use_mpegts(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoHlsUseMpegts);
        self
    }

    /// Name or path of the external downloader to use (optionally) prefixed by the protocols (http, ftp, m3u8, dash, rstp, rtmp, mms) to use it for. Currently supports native, aria2c, avconv, axel, curl, ffmpeg, httpie, wget (Recommended: aria2c). You can use this option multiple times to set different downloaders for different protocols. For example, --downloader aria2c --downloader "dash,m3u8:native" will use aria2c for http/ftp downloads, and the native downloader for dash/m3u8 downloads (Alias: --external-downloader)
    pub fn downloader(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Downloader(v.to_string()));
        self
    }

    /// Give these arguments to the external downloader. Specify the downloader name and the arguments separated by a colon ":". For ffmpeg, arguments can be passed to different positions using the same syntax as --postprocessor-args. You can use this option multiple times to give different arguments to different downloaders (Alias: --external-downloader-args)
    pub fn downloader_args(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::DownloaderArgs(v.to_string()));
        self
    }

    /// File containing URLs to download ("-" for stdin), one URL per line. Lines starting with "#", ";" or "]" are considered as comments and ignored
    pub fn batch_file(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::BatchFile(v.to_string()));
        self
    }

    /// Do not read URLs from batch file (default)
    pub fn no_batch_file(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoBatchFile);
        self
    }

    /// The paths where the files should be downloaded. Specify the type of file and the path separated by a colon ":". All the same TYPES as --output are supported. Additionally, you can also provide "home" (default) and "temp" paths. All intermediary files are first downloaded to the temp path and then the final files are moved over to the home path after download is finished. This option is ignored if --output is an absolute path
    pub fn paths(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Paths(v.to_string()));
        self
    }

    /// Output filename template; see "OUTPUT TEMPLATE" for details
    pub fn output(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Output(v.to_string()));
        self
    }

    /// Placeholder value for unavailable meta fields in output filename template (default: "NA")
    pub fn output_na_placeholder(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::OutputNaPlaceholder(v.to_string()));
        self
    }

    /// Restrict filenames to only ASCII characters, and avoid "&" and spaces in filenames
    pub fn restrict_filenames(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::RestrictFilenames);
        self
    }

    /// Allow Unicode characters, "&" and spaces in filenames (default)
    pub fn no_restrict_filenames(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoRestrictFilenames);
        self
    }

    /// Force filenames to be Windows-compatible
    pub fn windows_filenames(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WindowsFilenames);
        self
    }

    /// Make filenames Windows-compatible only if using Windows (default)
    pub fn no_windows_filenames(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWindowsFilenames);
        self
    }

    /// Limit the filename length (excluding extension) to the specified number of characters
    pub fn trim_filenames(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::TrimFilenames(v.to_string()));
        self
    }

    /// Do not overwrite any files
    pub fn no_overwrites(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoOverwrites);
        self
    }

    /// Overwrite all video and metadata files. This option includes --no-continue
    pub fn force_overwrites(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceOverwrites);
        self
    }

    /// Do not overwrite the video, but overwrite related files (default)
    pub fn no_force_overwrites(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoForceOverwrites);
        self
    }

    /// Resume partially downloaded files/fragments (default)
    pub fn continue_dl(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Continue);
        self
    }

    /// Do not resume partially downloaded fragments. If the file is not fragmented, restart download of the entire file
    pub fn no_continue(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoContinue);
        self
    }

    /// Use .part files instead of writing directly into output file (default)
    pub fn part(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Part);
        self
    }

    /// Do not use .part files - write directly into output file
    pub fn no_part(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoPart);
        self
    }

    /// Use the Last-modified header to set the file modification time (default)
    pub fn mtime(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Mtime);
        self
    }

    /// Do not use the Last-modified header to set the file modification time
    pub fn no_mtime(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoMtime);
        self
    }

    /// Write video description to a .description file
    pub fn write_description(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteDescription);
        self
    }

    /// Do not write video description (default)
    pub fn no_write_description(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteDescription);
        self
    }

    /// Write video metadata to a .info.json file (this may contain personal information)
    pub fn write_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteInfoJson);
        self
    }

    /// Do not write video metadata (default)
    pub fn no_write_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteInfoJson);
        self
    }

    /// Write playlist metadata in addition to the video metadata when using --write-info- json, --write-description etc. (default)
    pub fn write_playlist_metafiles(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WritePlaylistMetafiles);
        self
    }

    /// Do not write playlist metadata when using --write-info-json, --write-description etc.
    pub fn no_write_playlist_metafiles(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWritePlaylistMetafiles);
        self
    }

    /// Remove some private fields such as filenames from the infojson. Note that it could still contain some personal information (default)
    pub fn clean_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::CleanInfoJson);
        self
    }

    /// Write all fields to the infojson
    pub fn no_clean_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCleanInfoJson);
        self
    }

    /// Retrieve video comments to be placed in the infojson. The comments are fetched even without this option if the extraction is known to be quick (Alias: --get-comments)
    pub fn write_comments(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteComments);
        self
    }

    /// Do not retrieve video comments unless the extraction is known to be quick (Alias: --no-get-comments)
    pub fn no_write_comments(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteComments);
        self
    }

    /// JSON file containing the video information (created with the "--write-info-json" option)
    pub fn load_info_json(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::LoadInfoJson(v.to_string()));
        self
    }

    /// Netscape formatted file to read cookies from and dump cookie jar in
    pub fn cookies(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Cookies(v.to_string()));
        self
    }

    /// Do not read/dump cookies from/to file (default)
    pub fn no_cookies(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCookies);
        self
    }

    /// BROWSER[+KEYRING][:PROFILE] The name of the browser and (optionally) the name/path of the profile to load cookies from, separated by a ":". Currently supported browsers are: brave, chrome, chromium, edge, firefox, opera, safari, vivaldi. By default, the most recently accessed profile is used. The keyring used for decrypting Chromium cookies on Linux can be (optionally) specified after the browser name separated by a "+". Currently supported keyrings are: basictext, gnomekeyring, kwallet
    pub fn cookies_from_browser(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::CookiesFromBrowser);
        self
    }

    /// Do not load cookies from browser (default)
    pub fn no_cookies_from_browser(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCookiesFromBrowser);
        self
    }

    /// Location in the filesystem where youtube-dl can store some downloaded information (such as client ids and signatures) permanently. By default $XDG_CACHE_HOME/yt-dlp or ~/.cache/yt-dlp
    pub fn cache_dir(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::CacheDir(v.to_string()));
        self
    }

    /// Disable filesystem caching
    pub fn no_cache_dir(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCacheDir);
        self
    }

    /// Delete all filesystem cache files
    pub fn rm_cache_dir(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::RmCacheDir);
        self
    }

    /// Write thumbnail image to disk
    pub fn write_thumbnail(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteThumbnail);
        self
    }

    /// Do not write thumbnail image to disk (default)
    pub fn no_write_thumbnail(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteThumbnail);
        self
    }

    /// Write all thumbnail image formats to disk
    pub fn write_all_thumbnails(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteAllThumbnails);
        self
    }

    /// List available thumbnails of each video. Simulate unless --no-simulate is used
    pub fn list_thumbnails(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ListThumbnails);
        self
    }

    /// Write an internet shortcut file, depending on the current platform (.url, .webloc or .desktop). The URL may be cached by the OS
    pub fn write_link(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteLink);
        self
    }

    /// Write a .url Windows internet shortcut. The OS caches the URL based on the file path
    pub fn write_url_link(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteUrlLink);
        self
    }

    /// Write a .webloc macOS internet shortcut
    pub fn write_webloc_link(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteWeblocLink);
        self
    }

    /// Write a .desktop Linux internet shortcut
    pub fn write_desktop_link(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteDesktopLink);
        self
    }

    /// Activate quiet mode. If used with --verbose, print the log to stderr
    pub fn quiet(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Quiet);
        self
    }

    /// Ignore warnings
    pub fn no_warnings(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWarnings);
        self
    }

    /// Do not download the video and do not write anything to disk
    pub fn simulate(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Simulate);
        self
    }

    /// Download the video even if printing/listing options are used
    pub fn no_simulate(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoSimulate);
        self
    }

    /// Ignore "No video formats" error. Useful for extracting metadata even if the videos are not actually available for download (experimental)
    pub fn ignore_no_formats_error(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::IgnoreNoFormatsError);
        self
    }

    /// Throw error when no downloadable video formats are found (default)
    pub fn no_ignore_no_formats_error(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoIgnoreNoFormatsError);
        self
    }

    /// Do not download the video but write all related files (Alias: --no-download)
    pub fn skip_download(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::SkipDownload);
        self
    }

    /// Field name or output template to print to screen, optionally prefixed with when to print it, separated by a ":". Supported values of "WHEN" are the same as that of --use-postprocessor, and "video" (default). Implies --quiet and --simulate (unless --no-simulate is used). This option can be used multiple times
    pub fn print(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Print(v.to_string()));
        self
    }

    /// Append given template to the file. The values of WHEN and TEMPLATE are same as that of --print. FILE uses the same syntax as the output template. This option can be used multiple times
    pub fn print_to_file(&mut self, v0: &str, v1: &str) -> &mut Self {
        self.0
            .push(YtDlpOption::PrintToFile(v0.to_string(), v1.to_string()));
        self
    }

    /// Quiet, but print JSON information for each video. Simulate unless --no-simulate is used. See "OUTPUT TEMPLATE" for a description of available keys
    pub fn dump_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::DumpJson);
        self
    }

    /// Quiet, but print JSON information for each url or infojson passed. Simulate unless --no-simulate is used. If the URL refers to a playlist, the whole playlist information is dumped in a single line
    pub fn dump_single_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::DumpSingleJson);
        self
    }

    /// Force download archive entries to be written as far as no errors occur, even if -s or another simulation option is used (Alias: --force-download-archive)
    pub fn force_write_archive(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceWriteArchive);
        self
    }

    /// Output progress bar as new lines
    pub fn newline(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Newline);
        self
    }

    /// Do not print progress bar
    pub fn no_progress(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoProgress);
        self
    }

    /// Show progress bar, even if in quiet mode
    pub fn progress(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Progress);
        self
    }

    /// Display progress in console titlebar
    pub fn console_title(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ConsoleTitle);
        self
    }

    /// Template for progress outputs, optionally prefixed with one of "download:" (default), "download-title:" (the console title), "postprocess:",  or "postprocess-title:". The video's fields are accessible under the "info" key and the progress attributes are accessible under "progress" key. E.g.: --console-title --progress-template "download- title:%(info.id)s-%(progress.eta)s"
    pub fn progress_template(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ProgressTemplate(v.to_string()));
        self
    }

    /// Print various debugging information
    pub fn verbose(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Verbose);
        self
    }

    /// Print downloaded pages encoded using base64 to debug problems (very verbose)
    pub fn dump_pages(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::DumpPages);
        self
    }

    /// Write downloaded intermediary pages to files in the current directory to debug problems
    pub fn write_pages(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WritePages);
        self
    }

    /// Display sent and read HTTP traffic
    pub fn print_traffic(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PrintTraffic);
        self
    }

    /// Force the specified encoding (experimental)
    pub fn encoding(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Encoding(v.to_string()));
        self
    }

    /// Explicitly allow HTTPS connection to servers that do not support RFC 5746 secure renegotiation
    pub fn legacy_server_connect(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::LegacyServerConnect);
        self
    }

    /// Suppress HTTPS certificate validation
    pub fn no_check_certificates(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCheckCertificates);
        self
    }

    /// Use an unencrypted connection to retrieve information about the video (Currently supported only for YouTube)
    pub fn prefer_insecure(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PreferInsecure);
        self
    }

    /// Specify a custom HTTP header and its value, separated by a colon ":". You can use this option multiple times
    pub fn add_header(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::AddHeader(v.to_string()));
        self
    }

    /// Work around terminals that lack bidirectional text support. Requires bidiv or fribidi executable in PATH
    pub fn bidi_workaround(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::BidiWorkaround);
        self
    }

    /// Number of seconds to sleep between requests during data extraction
    pub fn sleep_requests(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SleepRequests(v.to_string()));
        self
    }

    /// Number of seconds to sleep before each download. This is the minimum time to sleep when used along with --max-sleep-interval (Alias: --min-sleep-interval)
    pub fn sleep_interval(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SleepInterval(v.to_string()));
        self
    }

    /// Maximum number of seconds to sleep. Can only be used along with --min-sleep- interval
    pub fn max_sleep_interval(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MaxSleepInterval(v.to_string()));
        self
    }

    /// Number of seconds to sleep before each subtitle download
    pub fn sleep_subtitles(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SleepSubtitles(v.to_string()));
        self
    }

    /// Video format code, see "FORMAT SELECTION" for more details
    pub fn format(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Format(v.to_string()));
        self
    }

    /// Sort the formats by the fields given, see "Sorting Formats" for more details
    pub fn format_sort(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::FormatSort(v.to_string()));
        self
    }

    /// Force user specified sort order to have precedence over all fields, see "Sorting Formats" for more details
    pub fn format_sort_force(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::FormatSortForce);
        self
    }

    /// Some fields have precedence over the user specified sort order (default), see "Sorting Formats" for more details
    pub fn no_format_sort_force(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoFormatSortForce);
        self
    }

    /// Allow multiple video streams to be merged into a single file
    pub fn video_multistreams(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::VideoMultistreams);
        self
    }

    /// Only one video stream is downloaded for each output file (default)
    pub fn no_video_multistreams(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoVideoMultistreams);
        self
    }

    /// Allow multiple audio streams to be merged into a single file
    pub fn audio_multistreams(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::AudioMultistreams);
        self
    }

    /// Only one audio stream is downloaded for each output file (default)
    pub fn no_audio_multistreams(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoAudioMultistreams);
        self
    }

    /// Prefer video formats with free containers over non-free ones of same quality. Use with "-S ext" to strictly prefer free containers irrespective of quality
    pub fn prefer_free_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PreferFreeFormats);
        self
    }

    /// Don't give any special preference to free containers (default)
    pub fn no_prefer_free_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoPreferFreeFormats);
        self
    }

    /// Check that the selected formats are actually downloadable
    pub fn check_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::CheckFormats);
        self
    }

    /// Check all formats for whether they are actually downloadable
    pub fn check_all_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::CheckAllFormats);
        self
    }

    /// Do not check that the formats are actually downloadable
    pub fn no_check_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoCheckFormats);
        self
    }

    /// List available formats of each video. Simulate unless --no-simulate is used
    pub fn list_formats(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ListFormats);
        self
    }

    /// If a merge is required (e.g. bestvideo+bestaudio), output to given container format. One of mkv, mp4, ogg, webm, flv. Ignored if no merge is required
    pub fn merge_output_format(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::MergeOutputFormat(v.to_string()));
        self
    }

    /// Write subtitle file
    pub fn write_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteSubs);
        self
    }

    /// Do not write subtitle file (default)
    pub fn no_write_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteSubs);
        self
    }

    /// Write automatically generated subtitle file (Alias: --write-automatic-subs)
    pub fn write_auto_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::WriteAutoSubs);
        self
    }

    /// Do not write auto-generated subtitles (default) (Alias: --no-write-automatic- subs)
    pub fn no_write_auto_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoWriteAutoSubs);
        self
    }

    /// List available subtitles of each video. Simulate unless --no-simulate is used
    pub fn list_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ListSubs);
        self
    }

    /// Subtitle format, accepts formats preference, for example: "srt" or "ass/srt/best"
    pub fn sub_format(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SubFormat(v.to_string()));
        self
    }

    /// Languages of the subtitles to download (can be regex) or "all" separated by commas. (Eg: --sub-langs "en.*,ja") You can prefix the language code with a "-" to exempt it from the requested languages. (Eg: --sub- langs all,-live_chat) Use --list-subs for a list of available language tags
    pub fn sub_langs(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SubLangs(v.to_string()));
        self
    }

    /// Login with this account ID
    pub fn username(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Username(v.to_string()));
        self
    }

    /// Account password. If this option is left out, yt-dlp will ask interactively
    pub fn password(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Password(v.to_string()));
        self
    }

    /// Two-factor authentication code
    pub fn twofactor(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Twofactor(v.to_string()));
        self
    }

    /// Use .netrc authentication data
    pub fn netrc(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Netrc);
        self
    }

    /// Location of .netrc authentication data; either the path or its containing directory. Defaults to ~/.netrc
    pub fn netrc_location(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::NetrcLocation(v.to_string()));
        self
    }

    /// Video password (vimeo, youku)
    pub fn video_password(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::VideoPassword(v.to_string()));
        self
    }

    /// Adobe Pass multiple-system operator (TV provider) identifier, use --ap-list-mso for a list of available MSOs
    pub fn ap_mso(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ApMso(v.to_string()));
        self
    }

    /// Multiple-system operator account login
    pub fn ap_username(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ApUsername(v.to_string()));
        self
    }

    /// Multiple-system operator account password. If this option is left out, yt-dlp will ask interactively
    pub fn ap_password(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ApPassword(v.to_string()));
        self
    }

    /// List all supported multiple-system operators
    pub fn ap_list_mso(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ApListMso);
        self
    }

    /// Convert video files to audio-only files (requires ffmpeg and ffprobe)
    pub fn extract_audio(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ExtractAudio);
        self
    }

    /// Specify audio format to convert the audio to when -x is used. Currently supported formats are: best (default) or one of aac|flac|mp3|m4a|opus|vorbis|wav|alac
    pub fn audio_format(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::AudioFormat(v.to_string()));
        self
    }

    /// Specify ffmpeg audio quality, insert a value between 0 (best) and 10 (worst) for VBR or a specific bitrate like 128K (default 5)
    pub fn audio_quality(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::AudioQuality(v.to_string()));
        self
    }

    /// Remux the video into another container if necessary (currently supported: mp4|mkv|flv |webm|mov|avi|mp3|mka|m4a|ogg|opus). If target container does not support the video/audio codec, remuxing will fail. You can specify multiple rules; Eg. "aac>m4a/mov>mp4/mkv" will remux aac to m4a, mov to mp4 and anything else to mkv.
    pub fn remux_video(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::RemuxVideo(v.to_string()));
        self
    }

    /// Re-encode the video into another format if re-encoding is necessary. The syntax and supported formats are the same as --remux- video
    pub fn recode_video(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::RecodeVideo(v.to_string()));
        self
    }

    /// Give these arguments to the postprocessors. Specify the postprocessor/executable name and the arguments separated by a colon ":" to give the argument to the specified postprocessor/executable. Supported PP are: Merger, ModifyChapters, SplitChapters, ExtractAudio, VideoRemuxer, VideoConvertor, Metadata, EmbedSubtitle, EmbedThumbnail, SubtitlesConvertor, ThumbnailsConvertor, FixupStretched, FixupM4a, FixupM3u8, FixupTimestamp and FixupDuration. The supported executables are: AtomicParsley, FFmpeg and FFprobe. You can also specify "PP+EXE:ARGS" to give the arguments to the specified executable only when being used by the specified postprocessor. Additionally, for ffmpeg/ffprobe, "_i"/"_o" can be appended to the prefix optionally followed by a number to pass the argument before the specified input/output file. Eg: --ppa "Merger+ffmpeg_i1:-v quiet". You can use this option multiple times to give different arguments to different postprocessors. (Alias: --ppa)
    pub fn postprocessor_args(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::PostprocessorArgs(v.to_string()));
        self
    }

    /// Keep the intermediate video file on disk after post-processing
    pub fn keep_video(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::KeepVideo);
        self
    }

    /// Delete the intermediate video file after post-processing (default)
    pub fn no_keep_video(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoKeepVideo);
        self
    }

    /// Overwrite post-processed files (default)
    pub fn post_overwrites(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::PostOverwrites);
        self
    }

    /// Do not overwrite post-processed files
    pub fn no_post_overwrites(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoPostOverwrites);
        self
    }

    /// Embed subtitles in the video (only for mp4, webm and mkv videos)
    pub fn embed_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::EmbedSubs);
        self
    }

    /// Do not embed subtitles (default)
    pub fn no_embed_subs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoEmbedSubs);
        self
    }

    /// Embed thumbnail in the video as cover art
    pub fn embed_thumbnail(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::EmbedThumbnail);
        self
    }

    /// Do not embed thumbnail (default)
    pub fn no_embed_thumbnail(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoEmbedThumbnail);
        self
    }

    /// Embed metadata to the video file. Also embeds chapters/infojson if present unless --no-embed-chapters/--no-embed-info-json are used (Alias: --add-metadata)
    pub fn embed_metadata(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::EmbedMetadata);
        self
    }

    /// Do not add metadata to file (default) (Alias: --no-add-metadata)
    pub fn no_embed_metadata(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoEmbedMetadata);
        self
    }

    /// Add chapter markers to the video file (Alias: --add-chapters)
    pub fn embed_chapters(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::EmbedChapters);
        self
    }

    /// Do not add chapter markers (default) (Alias: --no-add-chapters)
    pub fn no_embed_chapters(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoEmbedChapters);
        self
    }

    /// Embed the infojson as an attachment to mkv/mka video files
    pub fn embed_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::EmbedInfoJson);
        self
    }

    /// Do not embed the infojson as an attachment to the video file
    pub fn no_embed_info_json(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoEmbedInfoJson);
        self
    }

    /// Parse additional metadata like title/artist from other fields; see "MODIFYING METADATA" for details
    pub fn parse_metadata(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ParseMetadata(v.to_string()));
        self
    }

    /// Replace text in a metadata field using the given regex. This option can be used multiple times
    pub fn replace_in_metadata(&mut self, v0: &str, v1: &str, v2: &str) -> &mut Self {
        self.0.push(YtDlpOption::ReplaceInMetadata(
            v0.to_string(),
            v1.to_string(),
            v2.to_string(),
        ));
        self
    }

    /// Write metadata to the video file's xattrs (using dublin core and xdg standards)
    pub fn xattrs(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::Xattrs);
        self
    }

    /// Concatenate videos in a playlist. One of "never", "always", or "multi_video" (default; only when the videos form a single show). All the video files must have same codecs and number of streams to be concatable. The "pl_video:" prefix can be used with "--paths" and "--output" to set the output filename for the split files. See "OUTPUT TEMPLATE" for details
    pub fn concat_playlist(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ConcatPlaylist(v.to_string()));
        self
    }

    /// Automatically correct known faults of the file. One of never (do nothing), warn (only emit a warning), detect_or_warn (the default; fix file if we can, warn otherwise), force (try fixing even if file already exists)
    pub fn fixup(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Fixup(v.to_string()));
        self
    }

    /// Location of the ffmpeg binary; either the path to the binary or its containing directory
    pub fn ffmpeg_location(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::FfmpegLocation(v.to_string()));
        self
    }

    /// Execute a command, optionally prefixed with when to execute it (after_move if unspecified), separated by a ":". Supported values of "WHEN" are the same as that of --use-postprocessor. Same syntax as the output template can be used to pass any field as arguments to the command. After download, an additional field "filepath" that contains the final path of the downloaded file is also available, and if no fields are passed, %(filepath)q is appended to the end of the command. This option can be used multiple times
    pub fn exec(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Exec(v.to_string()));
        self
    }

    /// Remove any previously defined --exec
    pub fn no_exec(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoExec);
        self
    }

    /// Convert the subtitles to another format (currently supported: srt|vtt|ass|lrc) (Alias: --convert-subtitles)
    pub fn convert_subs(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ConvertSubs(v.to_string()));
        self
    }

    /// Convert the thumbnails to another format (currently supported: jpg|png|webp)
    pub fn convert_thumbnails(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ConvertThumbnails(v.to_string()));
        self
    }

    /// Split video into multiple files based on internal chapters. The "chapter:" prefix can be used with "--paths" and "--output" to set the output filename for the split files. See "OUTPUT TEMPLATE" for details
    pub fn split_chapters(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::SplitChapters);
        self
    }

    /// Do not split video based on chapters (default)
    pub fn no_split_chapters(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoSplitChapters);
        self
    }

    /// Remove chapters whose title matches the given regular expression. Time ranges prefixed by a "*" can also be used in place of chapters to remove the specified range. Eg: --remove-chapters "*10:15-15:00" --remove-chapters "intro". This option can be used multiple times
    pub fn remove_chapters(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::RemoveChapters(v.to_string()));
        self
    }

    /// Do not remove any chapters from the file (default)
    pub fn no_remove_chapters(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoRemoveChapters);
        self
    }

    /// Force keyframes around the chapters before removing/splitting them. Requires a re- encode and thus is very slow, but the resulting video may have fewer artifacts around the cuts
    pub fn force_keyframes_at_cuts(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::ForceKeyframesAtCuts);
        self
    }

    /// Do not force keyframes around the chapters when cutting/splitting (default)
    pub fn no_force_keyframes_at_cuts(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoForceKeyframesAtCuts);
        self
    }

    /// The (case sensitive) name of plugin postprocessors to be enabled, and (optionally) arguments to be passed to it, separated by a colon ":". ARGS are a semicolon ";" delimited list of NAME=VALUE. The "when" argument determines when the postprocessor is invoked. It can be one of "pre_process" (after video extraction), "after_filter" (after video passes filter), "before_dl" (before each video download), "post_process" (after each video download; default), "after_move" (after moving video file to it's final locations), "after_video" (after downloading and processing all formats of a video), or "playlist" (at end of playlist). This option can be used multiple times to add different postprocessors
    pub fn use_postprocessor(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::UsePostprocessor(v.to_string()));
        self
    }

    /// SponsorBlock categories to create chapters for, separated by commas. Available categories are all, default(=all), sponsor, intro, outro, selfpromo, preview, filler, interaction, music_offtopic, poi_highlight. You can prefix the category with a "-" to exempt it. See [1] for description of the categories. Eg: --sponsorblock-mark all,-preview [1] https://wiki.sponsor.ajay. app/w/Segment_Categories
    pub fn sponsorblock_mark(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SponsorblockMark(v.to_string()));
        self
    }

    /// SponsorBlock categories to be removed from the video file, separated by commas. If a category is present in both mark and remove, remove takes precedence. The syntax and available categories are the same as for --sponsorblock-mark except that "default" refers to "all,-filler" and poi_highlight is not available
    pub fn sponsorblock_remove(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SponsorblockRemove(v.to_string()));
        self
    }

    /// The title template for SponsorBlock chapters created by --sponsorblock-mark. The same syntax as the output template is used, but the only available fields are start_time, end_time, category, categories, name, category_names. Defaults to "[SponsorBlock]: %(category_names)l"
    pub fn sponsorblock_chapter_title(&mut self, v: &str) -> &mut Self {
        self.0
            .push(YtDlpOption::SponsorblockChapterTitle(v.to_string()));
        self
    }

    /// Disable both --sponsorblock-mark and --sponsorblock-remove
    pub fn no_sponsorblock(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoSponsorblock);
        self
    }

    /// SponsorBlock API location, defaults to https://sponsor.ajay.app
    pub fn sponsorblock_api(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::SponsorblockApi(v.to_string()));
        self
    }

    /// Number of retries for known extractor errors (default is 3), or "infinite"
    pub fn extractor_retries(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ExtractorRetries(v.to_string()));
        self
    }

    /// Process dynamic DASH manifests (default) (Alias: --no-ignore-dynamic-mpd)
    pub fn allow_dynamic_mpd(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::AllowDynamicMpd);
        self
    }

    /// Do not process dynamic DASH manifests (Alias: --no-allow-dynamic-mpd)
    pub fn ignore_dynamic_mpd(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::IgnoreDynamicMpd);
        self
    }

    /// Split HLS playlists to different formats at discontinuities such as ad breaks
    pub fn hls_split_discontinuity(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::HlsSplitDiscontinuity);
        self
    }

    /// Do not split HLS playlists to different formats at discontinuities such as ad breaks (default)
    pub fn no_hls_split_discontinuity(&mut self) -> &mut Self {
        self.0.push(YtDlpOption::NoHlsSplitDiscontinuity);
        self
    }

    /// Pass these arguments to the extractor. See "EXTRACTOR ARGUMENTS" for details. You can use this option multiple times to give arguments for different extractors
    pub fn extractor_args(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::ExtractorArgs(v.to_string()));
        self
    }

    /// video url
    pub fn url(&mut self, v: &str) -> &mut Self {
        self.0.push(YtDlpOption::Url(v.to_string()));
        self
    }

    /// ard mediathek video id
    pub fn mediathek_id(&mut self, id: &str) -> &mut Self {
        let url = format!("https://www.ardmediathek.de/video/{id}");
        self.0.push(YtDlpOption::Url(url));
        self
    }

    /// twitch video id
    pub fn twitch_id(&mut self, id: &str) -> &mut Self {
        let url = format!("https://www.twitch.tv/videos/{id}");
        self.0.push(YtDlpOption::Url(url));
        self
    }

    /// youtube video id
    pub fn youtube_id(&mut self, id: &str) -> &mut Self {
        self.0.push(YtDlpOption::Url(id.into()));
        self
    }

    /// Requires the command to complete before the specified duration has elapsed.
    ///
    /// If the command completes before the duration has elapsed, then its output is returned.
    /// Otherwise, an error is returned and the command is canceled.
    pub fn timeout(&mut self, timeout: std::time::Duration) -> &mut Self {
        self.1 = Some(timeout);
        self
    }

    pub async fn run(&mut self) -> Result<String, process::CommandError> {
        crate::run(std::mem::take(&mut self.0), self.1).await
    }
}
