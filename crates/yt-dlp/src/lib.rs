//! # Rust bindings for yt-dlp
//! # Dependencies:
//! - python3
//! - pip3
//! - yt-dlp
//! - ffmpeg
//! - ffprobe

mod ytdlp;

use std::time::Duration;

use itertools::Itertools;
pub use process::CommandError;
pub use ytdlp::{YtDlpOption, YtDlpProcess};

pub fn yt_dlp() -> YtDlpProcess {
    YtDlpProcess::default()
}

async fn run(
    options: Vec<YtDlpOption>,
    timeout: Option<Duration>,
) -> Result<String, process::CommandError> {
    let args = options
        .into_iter()
        // ensure the url is the last argument
        .sorted_by_key(|o| matches!(o, YtDlpOption::Url(_)))
        .flat_map(YtDlpOption::to_args);

    let mut cmd = process::Command::new("yt-dlp");
    cmd.args(args);
    if let Some(t) = timeout {
        cmd.timeout(t);
    }

    cmd.output_string().await.map_err(decode_aria2_errors)
}

pub async fn update() -> Result<String, process::CommandError> {
    process::Command::new("pip")
        .args(["install", "-U", "yt-dlp"])
        .output_string()
        .await
}

/// adds more detail to errors thrown by aria2
///
/// e.g.: `ERROR: aria2c exited with code 22` will become `aria2 error: HTTP response header was bad or unexpected.`
fn decode_aria2_errors(mut error: process::CommandError) -> process::CommandError {
    const ARIA2_EXIT_CODES :[&str; 33] = [
        "all downloads were successful.",
        "an unknown error occurred.",
        "time out occurred.",
        "a resource was not found.",
        "aria2 saw the specified number of \"resource not found\" error. See --max-file-not-found option.",
        "a download aborted because download speed was too slow. See --lowest-speed-limit option.",
        "network problem occurred.",
        "there were unfinished downloads. This error is only reported if all finished downloads were successful and there were unfinished downloads in a queue when aria2 exited by pressing Ctrl-C by an user or sending TERM or INT signal.",
        "remote server did not support resume when resume was required to complete download.",
        "there was not enough disk space available.",
        "piece length was different from one in .aria2 control file. See --allow-piece-length-change option.",
        "aria2 was downloading same file at that moment.",
        "aria2 was downloading same info hash torrent at that moment.",
        "file already existed. See --allow-overwrite option.",
        "renaming file failed. See --auto-file-renaming option.",
        "aria2 could not open existing file.",
        "aria2 could not create new file or truncate existing file.",
        "file I/O error occurred.",
        "aria2 could not create directory.",
        "name resolution failed.",
        "aria2 could not parse Metalink document.",
        "FTP command failed.",
        "HTTP response header was bad or unexpected.",
        "too many redirects occurred.",
        "HTTP authorization failed.",
        "aria2 could not parse bencoded file (usually \".torrent\" file).",
        "\".torrent\" file was corrupted or missing information that aria2 needed.",
        "Magnet URI was bad.",
        "bad/unrecognized option was given or unexpected option argument was given.",
        "the remote server was unable to handle the request due to a temporary overloading or maintenance.",
        "aria2 could not parse JSON-RPC request.",
        "Reserved. Not used.",
        "checksum validation failed.",
      ];

    if let process::CommandError::CommandSuccessError {
        source: process::StdErrorOutput(ref mut err),
    } = error
    {
        if let Some(msg) = err
            .strip_prefix("ERROR: aria2c exited with code ")
            .and_then(|code| {
                code.chars()
                    .filter(|c| c.is_numeric())
                    .collect::<String>()
                    .parse::<usize>()
                    .ok()
            })
            .and_then(|i| ARIA2_EXIT_CODES.get(i))
        {
            *err = format!("aria2 error: {msg}");
        }
    }
    error
}
