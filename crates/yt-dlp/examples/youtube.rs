use yt_dlp::yt_dlp;

const VIDEO_ID: &str = "dQw4w9WgXcQ";

#[tokio::main]
async fn main() {
    println!("downloading video from youtube:");
    let res = yt_dlp().url(VIDEO_ID).verbose().run().await;
    let output = match res {
        Ok(o) => o,
        Err(e) => e.to_string(),
    };
    println!("{output}");
}
