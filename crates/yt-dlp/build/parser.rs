use apply::Apply;
use nom::{
    bytes::complete::{is_a, is_not},
    character::complete::{char, space0},
    combinator::peek,
    multi::{separated_list0, separated_list1},
    IResult,
};

use super::{Arg, IArg::*};

pub(crate) fn line(s: &str) -> IResult<&str, Arg> {
    let (s, args) = args(s)?;
    let (s, _) = char(' ')(s)?;
    let (s, n) = params(s)?;
    let (desc, _) = space0(s)?;

    let name = args.iter().max_by_key(|s| s.len()).unwrap();
    let option_name = camel_case(name);
    let function_name = name
        .to_ascii_lowercase()
        .chars()
        .skip_while(|&c| c == '-')
        .map(|c| if c.is_ascii_alphanumeric() { c } else { '_' })
        .collect::<String>();
    let arg = args
        .iter()
        .min_by_key(|s| s.len())
        .unwrap()
        .clone()
        .apply(|a| if n == 0 { Simple(a) } else { Params(a, n) });

    let arg = Arg {
        option_name,
        function_name,
        arg,
        description: desc.to_string(),
    };

    Ok((s, arg))
}

fn args(s: &str) -> IResult<&str, Vec<String>> {
    let (s, args) = separated_list1(char(','), arg)(s)?;
    Ok((s, args))
}

fn arg(s: &str) -> IResult<&str, String> {
    let (s, _) = space0(s)?;
    let (s, _) = peek(char('-'))(s)?;
    let (s, arg) = is_not(", =[")(s)?;
    Ok((s, arg.to_string()))
}

fn params(s: &str) -> IResult<&str, usize> {
    let (s, p) = separated_list0(char(' '), param)(s)?;
    Ok((s, p.len()))
}

fn param(s: &str) -> IResult<&str, String> {
    let (s, param) = is_a("ABCDEFGHIJKLMNOPQRSTUVWXYZ[]:|")(s)?;
    let (s, _) = peek(char(' '))(s)?;
    Ok((s, param.to_string()))
}

fn camel_case(s: impl AsRef<str>) -> String {
    let mut chars = s.as_ref().chars().skip_while(|&c| c == '-');
    let mut res = chars.next().unwrap().to_uppercase().to_string();
    while let Some(c) = chars.next() {
        if c == '-' {
            res.push_str(&chars.next().unwrap().to_uppercase().to_string());
        } else {
            res.push(c);
        }
    }
    res
}
