mod args;
mod parser;

use std::fmt::Write;

#[derive(Debug, Clone)]
pub struct Arg {
    arg: IArg,
    option_name: String,
    function_name: String,
    description: String,
}

#[derive(Debug, Clone)]
pub enum IArg {
    Simple(String),
    Params(String, usize),
}

type Error = Box<dyn std::error::Error>;

pub fn code() -> Result<String, Error> {
    let args = args::args()?;
    let mut code = String::new();

    writeln!(
        code,
        "//! auto-generated code\n\
        //! do not edit"
    )?;
    writeln!(code)?;
    write_code_option_enum(&mut code, &args)?;
    writeln!(code)?;
    write_code_option_impl(&mut code, &args)?;
    writeln!(code)?;
    write_code_process(&mut code, &args)?;

    Ok(code)
}

fn write_code_option_enum(c: &mut String, args: &[Arg]) -> Result<(), std::fmt::Error> {
    writeln!(
        c,
        "#[derive(Clone, Debug)]\n\
        pub enum YtDlpOption{{"
    )?;
    for arg in args {
        write_code_option_variant(c, arg)?;
    }
    writeln!(c, "}}")
}

fn write_code_option_variant(
    c: &mut String,
    Arg {
        option_name,
        description,
        arg,
        ..
    }: &Arg,
) -> Result<(), std::fmt::Error> {
    writeln!(c, "/// {description}")?;
    write!(c, "{option_name}")?;
    if let &IArg::Params(_, n) = arg {
        let inner = (0..n).map(|_| "String").collect::<Vec<_>>().join(", ");
        write!(c, "({inner})")?;
    }
    writeln!(c, ",")
}

fn write_code_option_impl(c: &mut String, args: &[Arg]) -> Result<(), std::fmt::Error> {
    writeln!(
        c,
        "impl YtDlpOption {{\n\
            pub fn to_args(o: YtDlpOption) -> Vec<String> {{\n\
                use YtDlpOption::*;\n\
                match o {{"
    )?;
    for arg in args {
        write_code_option_impl_variant(c, arg)?;
    }
    writeln!(
        c,
        "       }}\n\
            }}\n\
        }}"
    )
}

fn write_code_option_impl_variant(
    c: &mut String,
    Arg {
        arg, option_name, ..
    }: &Arg,
) -> Result<(), std::fmt::Error> {
    match arg {
        IArg::Simple(a) => writeln!(c, "{option_name} => vec![\"{a}\".to_string()],"),
        IArg::Params(a, n) => {
            let vs = (0..*n)
                .map(|i| format!("v{i}"))
                .collect::<Vec<_>>()
                .join(", ");
            writeln!(c, "{option_name}({vs}) => vec![\"{a}\".to_string(), {vs}],")
        }
    }
}

fn write_code_process(c: &mut String, args: &[Arg]) -> Result<(), std::fmt::Error> {
    writeln!(
        c,
        "#[derive(Clone, Debug, Default)]\n\
        pub struct YtDlpProcess(Vec<YtDlpOption>, Option<std::time::Duration>);\n\
        \n\
        impl YtDlpProcess {{"
    )?;

    for arg in args {
        write_code_process_function(c, arg)?;
    }

    // pub fn ard(video_id: &str, base_filename: &Utf8Path, directory: &Utf8Path) -> DownloadProcess {
    //     DownloadProcess::new(
    //         &format!("https://www.ardmediathek.de/video/{video_id}"),
    //         base_filename,
    //         directory,
    //     )
    // }

    // pub fn twitch(video_id: &str, base_filename: &Utf8Path, directory: &Utf8Path) -> DownloadProcess {
    //     DownloadProcess::new(
    //         &format!("https://www.twitch.tv/videos/{video_id}"),
    //         base_filename,
    //         directory,
    //     )
    // }

    writeln!(
        c,
            "/// ard mediathek video id\n\
            pub fn mediathek_id(&mut self, id: &str) -> &mut Self {{\n\
                let url = format!(\"https://www.ardmediathek.de/video/{{id}}\");\n\
                self.0.push(YtDlpOption::Url(url));\n\
                self\n\
            }}\n\
            \n\
            /// twitch video id\n\
            pub fn twitch_id(&mut self, id: &str) -> &mut Self {{\n\
                let url = format!(\"https://www.twitch.tv/videos/{{id}}\");\n\
                self.0.push(YtDlpOption::Url(url));\n\
                self\n\
            }}\n\
            \n\
            /// youtube video id\n\
            pub fn youtube_id(&mut self, id: &str) -> &mut Self {{\n\
                self.0.push(YtDlpOption::Url(id.into()));\n\
                self\n\
            }}\n\
            \n\
            /// Requires the command to complete before the specified duration has elapsed.\n\
            /// \n\
            /// If the command completes before the duration has elapsed, then its output is returned.\n\
            /// Otherwise, an error is returned and the command is canceled.\n\
            pub fn timeout(&mut self, timeout: std::time::Duration) -> &mut Self {{\n\
                self.1 = Some(timeout);\n\
                self\n\
            }}\n\
            \n\
            pub async fn run(&mut self) -> Result<String, process::CommandError> {{\n\
                crate::run(std::mem::take(&mut self.0), self.1).await\n\
            }}\n\
        }}")
}

fn write_code_process_function(
    c: &mut String,
    Arg {
        arg,
        option_name,
        function_name,
        description,
    }: &Arg,
) -> Result<(), std::fmt::Error> {
    let function_name = if function_name == "continue" {
        "continue_dl"
    } else {
        function_name
    };
    writeln!(c, "/// {description}")?;
    match *arg {
        IArg::Simple(_) => {
            writeln!(c, "pub fn {function_name}(&mut self) -> &mut Self {{")?;
            writeln!(c, "self.0.push(YtDlpOption::{option_name});")?;
        }
        IArg::Params(_, n) => {
            if n == 1 {
                writeln!(
                    c,
                    "pub fn {function_name}(&mut self, v: &str) -> &mut Self {{"
                )?;
                writeln!(c, "self.0.push(YtDlpOption::{option_name}(v.to_string()));")?;
            } else {
                let vs = (0..n)
                    .map(|i| format!("v{i}: &str"))
                    .collect::<Vec<_>>()
                    .join(", ");
                let vs_to_string = (0..n)
                    .map(|i| format!("v{i}.to_string()"))
                    .collect::<Vec<_>>()
                    .join(", ");
                writeln!(c, "pub fn {function_name}(&mut self, {vs}) -> &mut Self {{")?;
                writeln!(
                    c,
                    "self.0.push(YtDlpOption::{option_name}({vs_to_string}));"
                )?;
            }
        }
    }
    writeln!(c, "self")?;
    writeln!(c, "}}")?;
    writeln!(c)
}
