use crate::YouTubeError;
use apply::Apply;
use atom_syndication::{Entry, Feed};
use chrono::{Duration, Utc};
use network::Client;
use sm_api::model::YouTubeVideo;

pub async fn get(
    channel_id: &str,
    max_days: u32,
    max_num: u32,
) -> Result<Vec<YouTubeVideo>, YouTubeError> {
    let client = network::new_client()?;
    get_with_client(&client, channel_id, max_days, max_num).await
}

pub async fn get_with_client(
    client: &Client,
    channel_id: &str,
    max_days: u32,
    max_num: u32,
) -> Result<Vec<YouTubeVideo>, YouTubeError> {
    let feed_url = format!("https://www.youtube.com/feeds/videos.xml?channel_id={channel_id}");

    let feed = (&feed_url)
        .apply(|feed_url| network::download_text(client, feed_url))
        .await?
        .parse::<Feed>()
        .map_err(|source| YouTubeError::FeedParseError {
            source,
            url: feed_url.to_string(),
        })?;

    let channel = feed.title().to_string();

    let now = Utc::now();

    let videos = feed
        .entries()
        .iter()
        .take(max_num.try_into().unwrap_or_default())
        .filter_map(|entry| parse_item(entry, &channel))
        // fewer than `days` old
        .filter(|i| {
            i.upload_date
                .checked_add_signed(Duration::days(max_days.into()))
                .unwrap_or(i.upload_date)
                > now
        })
        .collect();

    Ok(videos)
}

fn parse_item(entry: &Entry, channel: &str) -> Option<YouTubeVideo> {
    // get published date / ignore older videos
    let upload_date = (*entry.published()?).into();
    let id = entry
        .extensions()
        .get("yt")?
        .get("videoId")?
        .first()?
        .value()?
        .to_string();
    let description = entry
        .extensions()
        .get("media")?
        .get("group")?
        .first()?
        .children()
        .get("description")?
        .first()?
        .value()?
        .to_string();
    Some(YouTubeVideo {
        id,
        channel: channel.to_string(),
        title: entry.title().to_string(),
        description,
        upload_date,
    })
}
