use google_api::{TokenStorage, VideoInfo};
use sm_api::model::YouTubeVideo;

use crate::YouTubeError;

pub async fn subscribed_channels(
    client_secret: &str,
    token_storage: Box<dyn TokenStorage>,
) -> Result<Vec<String>, YouTubeError> {
    let channels = google_api::YouTubeApi::new(client_secret, token_storage)
        .await?
        .subscribed_channels()
        .await?;
    Ok(channels)
}

/// gets all uploads from all subscribed channels (max `n` videos per channel that are at most `days` old)
///
/// tries to get as many videos as possible and will keep going even if some requests fail
///
/// # Warning
/// this could incur a horrendous quota cost
pub async fn subscription_uploads(
    client_secret: &str,
    token_storage: Box<dyn TokenStorage>,
    max_days: u32,
    max_num_per_channel: u32,
) -> Result<Vec<YouTubeVideo>, YouTubeError> {
    let videos = google_api::YouTubeApi::new(client_secret, token_storage)
        .await?
        .subscription_uploads(max_days, max_num_per_channel)
        .await?
        .into_iter()
        .map(from_info) // fewer than `days` old
        .collect();
    Ok(videos)
}

/// gets videos from playlist
pub async fn playlist_items(
    client_secret: &str,
    token_storage: Box<dyn TokenStorage>,
    playlist_name: &str,
) -> Result<Vec<(String, YouTubeVideo)>, YouTubeError> {
    let api = google_api::YouTubeApi::new(client_secret, token_storage).await?;
    let playlist_id = api.playlist_id(playlist_name).await?;
    let playlist_items = api
        .playlist_items(&playlist_id, 50)
        .await?
        .into_iter()
        .map(|(id, info)| (id, from_info(info)))
        .collect();
    Ok(playlist_items)
}

pub async fn remove_playlist_item(
    client_secret: &str,
    token_storage: Box<dyn TokenStorage>,
    playlist_item_id: &str,
) -> Result<(), YouTubeError> {
    google_api::YouTubeApi::new(client_secret, token_storage)
        .await?
        .delete_playlist_item(playlist_item_id)
        .await?;
    Ok(())
}

fn from_info(
    VideoInfo {
        channel,
        title,
        description,
        upload_date,
        id,
    }: VideoInfo,
) -> YouTubeVideo {
    YouTubeVideo {
        channel,
        title,
        description,
        upload_date,
        id,
    }
}
