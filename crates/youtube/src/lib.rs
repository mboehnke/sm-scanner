pub mod api;
pub mod rss;

use google_api::GoogleApiError;
pub use google_api::{TokenInfo, TokenStorage};
use network::NetworkError;
use thiserror::Error;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum YouTubeError {
    #[error(transparent)]
    NetworkError(#[from] NetworkError),

    #[error(transparent)]
    GoogleApiError(#[from] GoogleApiError),

    #[error("could not download rss feed: {url:?}")]
    FeedDownloadError {
        source: network::NetworkError,
        url: String,
    },

    #[error("could not parse rss feed: {url:?}")]
    FeedParseError {
        source: atom_syndication::Error,
        url: String,
    },

    #[error("failed to extract youtube id from url: {0:?}")]
    NoIdError(String),
}
