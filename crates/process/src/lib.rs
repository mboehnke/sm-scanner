//! thin wrapper around tokio::process::Command
//!
//! adds a timeout option and a simplified way to read data written by the external process

use std::{ffi::OsStr, path::Path, process::Stdio, time::Duration};

use apply::Apply;
use thiserror::Error;
use tokio::process::Child;

pub struct Command {
    command: tokio::process::Command,
    timeout: Option<Duration>,
}

/// CommandError enumerates all possible errors returned by this library.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum CommandError {
    #[error("failed to execute process")]
    CommandExecuteError { source: std::io::Error },

    #[error("command did not finish sucessfully")]
    CommandSuccessError { source: StdErrorOutput },

    #[error("command timed out after {0:?}")]
    CommandTimeoutError(Duration),

    #[error("output contained invalid UTF-8 sequence")]
    InvalidUtf8Error {
        #[from]
        source: std::string::FromUtf8Error,
    },

    #[error("could not capture stdout")]
    CommandOutputError,
}

// only here to store the actual output for CommandSuccessError
// should not be used on it's own
#[derive(Error, Debug)]
#[error("{0}")]
pub struct StdErrorOutput(pub String);

impl Command {
    pub fn new(program: impl AsRef<OsStr>) -> Self {
        let command = tokio::process::Command::new(program);
        let timeout = None;
        Command { command, timeout }
    }

    pub fn timeout(&mut self, timeout: Duration) -> &mut Self {
        self.timeout = Some(timeout);
        self
    }

    pub fn arg(&mut self, arg: impl AsRef<OsStr>) -> &mut Self {
        self.command.arg(arg);
        self
    }

    pub fn args<I, S>(&mut self, args: I) -> &mut Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        self.command.args(args);
        self
    }

    pub fn current_dir(&mut self, dir: impl AsRef<Path>) -> &mut Self {
        self.command.current_dir(dir);
        self
    }

    pub fn stdin(&mut self, cfg: impl Into<Stdio>) -> &mut Self {
        self.command.stdin(cfg);
        self
    }

    /// executes a Command and wraps it's output into a Result
    ///
    /// if a timeout is set and the process doesn't finish within this time
    /// the process will be killed and an error returned
    ///
    /// Ok() will contain output written to stdout,
    /// Err() will contain output written to stderr
    pub async fn output(&mut self) -> Result<Vec<u8>, CommandError> {
        let future = self
            .command
            .kill_on_drop(true)
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output();

        let output = if let Some(duration) = self.timeout {
            tokio::time::timeout(duration, future)
                .await
                .map_err(|_| CommandError::CommandTimeoutError(duration))?
        } else {
            future.await
        }
        .map_err(|source| CommandError::CommandExecuteError { source })?;

        if output.status.success() {
            Ok(output.stdout)
        } else {
            let source = output
                .stderr
                .as_slice()
                .apply(String::from_utf8_lossy)
                .to_string()
                .apply(StdErrorOutput);
            CommandError::CommandSuccessError { source }.apply(Err)
        }
    }

    /// executes a Command and wraps it's output into a Result
    ///
    /// if a timeout is set and the process doesn't finish within this time
    /// the process will be killed and an error returned
    ///
    /// Ok() will contain output written to stdout,
    /// Err() will contain output written to stderr
    ///
    /// # Warning
    /// this will return an error if output contains invalid UTF-8 sequences!
    pub async fn output_string(&mut self) -> Result<String, CommandError> {
        self.output()
            .await?
            .apply(String::from_utf8)?
            .trim()
            .to_string()
            .apply(Ok)
    }

    pub fn child_process(&mut self) -> Result<Child, CommandError> {
        self.command
            .kill_on_drop(false)
            .stdout(Stdio::piped())
            .spawn()
            .map_err(|source| CommandError::CommandExecuteError { source })
    }
}
