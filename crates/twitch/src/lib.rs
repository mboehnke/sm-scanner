use chrono::{DateTime, NaiveDateTime, Utc};
use sm_api::model::TwitchVideo;
pub use yt_dlp::CommandError;

/// gets up to `n` videos of `channel` that are at most `days` old
pub async fn get(
    channel: &str,
    max_days: u32,
    max_num: u32,
) -> Result<Vec<TwitchVideo>, CommandError> {
    let url = format!("https://www.twitch.tv/{channel}/videos?filter=all&sort=time");
    let date = format!("now-{max_days}days");
    let raw = yt_dlp::yt_dlp()
        .print("%(timestamp)s,%(id)s,%(title)s")
        .dateafter(&date)
        .playlist_end(&max_num.to_string())
        .url(&url)
        .run()
        .await?;
    let videos = raw
        .lines()
        .flat_map(|line| parse_line(line, channel))
        .collect();
    Ok(videos)
}

fn parse_line(line: &str, channel: &str) -> Option<TwitchVideo> {
    let mut fields = line.splitn(3, ',');
    let upload_date = DateTime::<Utc>::from_utc(
        NaiveDateTime::from_timestamp(fields.next()?.parse().ok()?, 0),
        Utc,
    );
    let id = fields.next()?.trim_start_matches('v').to_string();
    let title = fields.next()?.to_string();
    Some(TwitchVideo {
        channel: channel.to_string(),
        title,
        upload_date,
        id,
    })
}
